# Introdução

## Contexto da pandemia

A eclosão da epidemia do CoronaVirus atingiu o Brasil em meados de março de 2020, até o presente momento (Janeiro de 2021) foram 8.573.864 casos e 211.491 óbitos, o que fez com que o ano de 2020 possa ser considerado o ano de maior mortalidade do país. Os impactos da pandemia permanecem enormes em diversas esferas: social, economica e ambiental. Lockdowns constantes, suspensão das aulas nas escolas e sistemas de saúde na iminência do colapso. Mesmo com a chegada das vacinas, o mundo e o estilo de vida pré-pandemia ainda parecem distante para a maioria dos países.

A doença COVID-19, causada pelo coronavírus, é transmitida através do contato com gotículas de pacientes contaminados, sendo mais contagiosa e mais letal do que uma gripe comum. Apesar da maioria dos infectados desenvolverem quadros leves, uma parcela pode desenvolver sintomas mais severos necessitando respirar oxigênio, uma parcela menor ainda, pode requerer cuidados em uma unidade de tratamento intensiva (UTI). O diagnóstico mais comum em pacientes graves com COVID-19 é pneumonia grave [Orientação provisória WHO](https://apps.who.int/iris/bitstream/handle/10665/331446/WHO-2019-nCoV-clinical-2020.4-eng.pdf?sequence=1&isAllowed=y). 

## Duas ferramentas para diagnóstico: o PCR e o CT scan

Atualmente existem dois métodos para se fazer o diagnóstico. Cada qual com as suas vantagens e desvantagens.

O método padronizado para detectar o vírus, é o exame RT-PCR. O procedimento desse exame consiste em coletar amostras presentes na garganta, através de um cotonete inserido pelo nariz. Ele consegue identificar o vírus e dessa forma confirmar a doença Covid. Um dos principais problemas com esse método é o fato de ser laboratorial e exigir no mínimo dois dias para entregar os resultados. Outro problema é a disponibilidade, visto que existe uma alta demanda por esse exame no presente momento.

Um método alternativo é utilizar a tomografia computadorizada (CT scan) da região torácica. A tomografia pode ser comparada a uma espécie de raio-x tridimensional, ou seja, são geradas diversas imagens seccionando o tórax do paciente e essas imagens são agrupadas por um computador produzindo um volume que permite a visualização dos órgãos internos. Com auxílio do quadro de sintomas, do histórico do paciente, um radiologista consegue determinar se o caso se trata de covid. A vantagem do CT scan é a sua velocidade de diagnóstico. 

Existem discussões sobre a eficácia da utilização de CT scans para identificação de casos positivos de Covid [](https://www.cochrane.org/CD013639/INFECTN_how-accurate-chest-imaging-diagnosing-covid-19), alega-se dificuldade em separar a Covid de outras doenças. De qualquer forma, o CT scan funcionaria bem para identificar casos negativos. 

## Descrição das imagens com covid

Como a doença da Covid aparece nas imagens de tomografia? 

A característica principal para o diagnóstico no CT scan é a presença da opacidade do vidro fosco (ground glass opacities) no interior dos pulmões.

<p align="center">
	<img src="assets/Lung_CT-Scroll_through_COVID.jpg" alt="photo not available">
	<br>
	<em>A região mais esbranquiçada dentro da cavidade do pulmão é chamada opacidade de vidro fosco, indica a possibilidade de uma infecção por Covid.</em>
</p>

<p align="center">
	<img src="assets/health_vs_covid_lung.png" alt="photo not available">
	<br>
	<em>À esquerda, pulmão saudável, à direita, pulmão com covid (reparar nas estruturas de vidro fosco)</em>
</p>

## A contribuição do deep learning 

Com o crescimento das grandes plataformas e dos aplicativos nos smartphones uma grande quantidade de dados dos usuários passou a ser coletada e armazenada. Processar, analisar e extrair informações dessas massas de dados virou o passo fundamental de novos modelos de negócios. Os dados viraram o petróleo do século XXI. Por esse motivo, na década passada, os métodos de machine learning se expandiram para todas as áreas do conhecimento.

Para o modelo "aprender" com os dados, usando as técnicas de machine learning tradicionais, exige-se uma boa base de dados e a escolha de boas features. O machine learning tradicional funcionou muito bem para determinadas tarefas, mas não apresentava bons resultados para outras, por exemplo, reconhecimento de fala e reconhecimento de objetos.

Com esse cenário de fundo, com abundância de dados e capacidade de processamento, uma técnica nova começa a se destacar nas competição de machine learning, conhecida como deep learning (aprendizado profundo). O deep learning usa modelos baseados em redes neurais (não linear) e dispensa a etapa de seleção de features (bastante importante no machine learning tradicional) e apresenta ótimos resultados nas tarefas onde machine learning não tinha um bom desempenho.  

Com o uso crescente do deep learing para problemas de visão computacional, sua aplicação para diagnósticos em imagens médicas se mostra bastante promissora. Deep learning tem se mostrado a melhor opção para os problemas de classificação, segmentação e detecção de objetos. Existem muitos artigos usando deep learning para classificação de imagens de CT scan do tórax na internet.

A qualidade de um modelo de deep learning vai depender de 3 fatores: base de dados (qualidade e quantidade), arquitetura da rede e seleção de parâmetros. Como os dois últimos dependem apenas de pequenos ajustes, o maior limitante é sem dúvida a base de dados. Encontrar uma base de dados grande e corretamente rotulada não é coisa simples.

## Objetivos

O principal objetivo desse estudo é verificar o desempenho de neurais convolucionais treinadas em grandes conjuntos de imagens de CT scan em diagnósticos de imagens de exames reais. 

# Datasets

Em setembro de 2020 foi feita uma pesquisa online em busca dos melhores datasets para realizar um treinamento de modelos profundos. Chegou-se nos dois conjuntos abaixo, como aqueles que mais tinha imagens disponíveis classificadas para treinamento no momento.

## [COVIDx CT Dataset](https://github.com/haydengunraj/COVIDNet-CT/blob/master/docs/dataset.md)

O conjunto de dados COVIDx-CT é utilizado para o desenvolvimento da rede [COVIDNET-CT](https://github.com/haydengunraj/COVIDNet-CT). Utiliza diversas fontes para construir uma base de imagens (arquivos PNG, JPg, ...) bastante ampla. É um conjunto que utiliza três classes para classificar as imagens: covid, pneumonia normal e saudável. Possuí mais de 104.000 imagens (slices de CT scan) de 1489 pacientes.

<p align="center">
	<img src="assets/covidx-ct-dataset.png" alt="photo not available" width="70%" height="70%">
	<br>
	<em>Acima: Distribuição de treinamento, validação e teste; Abaixo: Distribuição dos pacientes; referentes a base do COVIDx-CT</em>
</p>

<p align="center">
	<img src="assets/covidx_3_classes.png" alt="photo not available">
	<br>
	<em>Um exemplo de cada classe (CP, NCP e Normal) presente no conjunto de dados</em>
</p>

Papers:

[CovidNet Chest X-ray](assets/paper_covidnet_chest.pdf)

[CovidNet Chest CT-Scan](assets/paper_covidnet_ct.pdf)

[Squeeze with CovidX dataset](assets/squeezenet_covidnet_ct.pdf)

## [MOSMED Data](https://mosmed.ai/)

O conjunto de dados Mosmed, disponibiliza exames de CT scan com presenção de Covid e casos saudáveis, são arquivos Nifti (a estrutura interna do exame é preservada, sua ordem e seus metadados). No total são exames de 1110 pacientes. Eles também disponibilizam máscaras de segmentação anotadas para alguns casos indicando onde estão as opacidades de vidro fosco. Esses exames foram disponibilizadas pelos hospitais de Moscou.

<div class="tg-wrap"><table class="tg">
  <tr>
    <th class="tg-7btt">Propriedade</th>
    <th class="tg-7btt">Valor</th>
  </tr>
  <tr>
    <td class="tg-7btt">Número de pacientes</td>
    <td class="tg-c3ow">1110</td>
  </tr>
  <tr>
    <td class="tg-7btt">Distribuição por sexo % (M/ F/ O)</td>
    <td class="tg-c3ow">42/ 56/ 2</td>
  </tr>
  <tr>
    <td class="tg-7btt">Distribuição por idade % (Mínimo/ Mediana/ Máximo)</td>
    <td class="tg-c3ow">18/ 47/ 97</td>
  </tr>
  <tr>
    <td class="tg-7btt">Número de mascáras binárias (Classe A)</td>
    <td class="tg-c3ow">50</td>
  </tr>
  <tr>
    <td class="tg-7btt">Número de estudos de cada categoria (CT-0/ CT-1/ CT-2/ CT-3/ CT-4)</td>
    <td class="tg-c3ow">254/ 684/ 125/ 45/ 2</td>
  </tr>
</table></div>

Pdfs:

[MosMed preprint](assets/mosmed_dataset.pdf)

[MosMed Readme EN](assets/README_EN_2_mosmed.pdf)

## Questionamentos?

Alguns pontos a serem considerados sobre os conjuntos.

### COVIDx

A qualidade e confiabilidade das imagens. Existe grande heterogeneidade nas imagens. Existem pacientes com mais do que um exame. A quantidade de imagens por exame varia de 1 a 363 imagens.

<p align="center">
	<img src="assets/covidx_data1.png" alt="photo not available">
	<br>
	<em>Exemplo de conjunto completo de dois exames</em>
</p>

### Mosmed

O conjunto COVIDx, possui como unidade de aprendizado a imagem, ou seja, as imagens que apresentam covid são verificadas e confirmadas por especialistas.

O conjunto Mosmed, possui como unidade de aprendizado o exame, pois a informação que se tem é sobre o exame e não sobre alguma imagem específica do exame. Por esse motivo, não é possível lidar com os conjuntos da mesma maneira, ou seja, usando um classificador de imagens 2D para ambos. 


# Modelos

Inicialmente, com os primeiros experimentos, se tinha o intuito de verificar o quanto uma determinada arquitetura poderia influenciar no desempenho do modelo, principalmente na sua capacidade generalizadora. Sabe-se que a capacidade de uma rede neural, o tipo de padrão que ela consegue aprender, varia de acordo com a quantidade de neurônios (ou parâmetros/pesos) e como as camadas desses neurônios estão conectadas. 

Pode-se melhorar a capacidade de uma rede, aumentando a quantidade de parâmetros em uma camada (aumentando a "largura" da rede) ou aumentando o número de camadas (aumentando a profundidade da rede). Redes largas, aprendem melhor relações lineares, redes profundas se saem melhor com relações não linerares.  

De maneira geral, foram testadas 4 arquiteturas: ResNet, VGG16 e duas variações de uma arquitura rasa, porém com bastante parâmetros (com "largura"). A ideia é verificar o desempenho entre esses modelos contrastantes.

É importante notar que existem duas possíveis abordagens com as imagens de um CT scan.

1. olhar os exames como unidade de informação, ou seja, tratar o conjunto das imagens como um volume e obter resposta para a questão "Esse exame mostra um pulmão saudável ou infectado?". Seria uma abordagem volumétrica (3D) das imagens;
2. olhar as imagens dos exames como unidade de informação e considera-las independentes, nesse caso, responde a questão "Essa imagem mostra um pulmão saudável ou infectado?". Seria uma abordagem de fatiar o exame em diversas imagens (2D) para dar de entrada ao treinamento.

A diferença principal é que assumindo a estrutura 3D estamos incorporando a dependência espacial entre as fatias, isso incorpora bastante informação ao treinamento, a parte negativa é que poucos exames são completos e apresentam sua estrutura 3D inteira.

Nos nossos experimentos, nos baseamos na abordagem 2, lidamos com as imagens com uma rede convoluional 2D.

## Rede rasa porém com largura (pouca profundidade, muitos parâmetros)

Segue uma representação esquemática do modelo.


Model: "sequential_1"
_________________________________________________________________
Layer (type)                 Output Shape              Param \#   
\=================================================================

conv2d_1 (Conv2D)            (None, 510, 510, 32)      320       
_________________________________________________________________
max_pooling2d_1 (MaxPooling2 (None, 255, 255, 32)      0         
_________________________________________________________________
conv2d_2 (Conv2D)            (None, 253, 253, 64)      18496     
_________________________________________________________________
max_pooling2d_2 (MaxPooling2 (None, 126, 126, 64)      0         
_________________________________________________________________
conv2d_3 (Conv2D)            (None, 124, 124, 128)     73856     
_________________________________________________________________
max_pooling2d_3 (MaxPooling2 (None, 62, 62, 128)       0         
_________________________________________________________________
conv2d_4 (Conv2D)            (None, 60, 60, 128)       147584    
_________________________________________________________________
max_pooling2d_4 (MaxPooling2 (None, 30, 30, 128)       0         
_________________________________________________________________
flatten_1 (Flatten)          (None, 115200)            0         
_________________________________________________________________
dense_1 (Dense)              (None, 512)               58982912  
_________________________________________________________________
dense_2 (Dense)              (None, 3)                 1539      
\=================================================================

Total params: 59,224,707
Trainable params: 59,224,707
Non-trainable params: 0
_________________________________________________________________

Nos experimentos também foi utilizada uma versão com Dropout do mesmo modelo. A camada de Dropout foi inserida logo após a camada flatten_1


# Experimentos

Foram realizados diversos experimentos, com diferentes finalidades. Para cada experimento foi criado um subconjunto, que foi amostrado do conjunto completo de imagens disponibilizados pelo COVIDx. Pode-se considerar o tamanho dos conjuntos de treinamento, validação e teste como 1200, 300 e 300 imagens. Estão presentes 3 classes, seguindo o que foi apresentado pelo COVIDx, CP - imagens de Covid, NCP - imagens de Pneumonia e Normal - imangens de pulmão saudável. 

Para registrar os experimentos, foi utilizado o framework [MlFlow](https://mlflow.org/).

1. **Experimento 1**: Primeiro experimento foi realizado com uma amostragem do conjunto completo dos dados. Foram criados três subgrupos (1100 para treinamento, 700 para validação e 700 para teste), separando as imagens por exames, ou seja, os conjuntos de treinamento/validação/teste não misturavam imagens dos exames e não eram balanceados.

2. **Experimento 2**: Esse experimento corrige o problema do desbalanceamento do experimento 1. Todos os subgrupos ficaram com classes balanceadas.

3. **Experimento 3**: Experimento tentou verificar a influência de diversos parâmetros: arquitetura de redes, camadas dropout e batchnorm, data augmentation e crop de bound-box.

Também tentou se observar mudança de comportamento dos valores de validação com aumento do conjunto de treinamento, ou seja, a duplicação do tamanho dos conjuntos.

Rodadas mais interessantes:

* 30ecdd0 -> menor loss
* 3edab06 -> menor validation loss

4. **Experimento 4 e 5**: Primeiro experimento para tentar entender melhor as imagens de entrada.

O conjunto (toy_data4) foi clusterizado, primeiro foi feita uma extração de features com a rede VGG16 e depois foram usados diversos algortimos para clusterizar os vetores. No final, foram gerados 3 clusters:
* imagens de pulmão alongada e com cavidade bem pequena, corresponde a parte superior do pulmão
* imagens de pulmão redonda e com cavidade bem pequena, corresponde a parte inferior do pulmão
* imagens de pulmão com cavidades bem visiveis, corresponde ao meio do pulmão

<p align="center">
	<img src="assets/toy_dataset4_clusters.png" alt="photo not available">
	<br>
	<em>Exemplo das imagens para cada cluster. À esquerda, imagem da parte superior do pulmão. Ao centro, imagem da parte média do pulmão. À direita, imagem da parte inferior do pulmão.</em>
</p>


Também foi criado um conjunto manualmente (toy_data5) com as melhores imagens para ver o desempenho dos modelos, uma seleção manual, retirando os erros da clusterização.

[loss exp5](assets/exp5_loss.png)

[acuracy exp5](assets/exp5_acc.png)

6. **Experimento 6**:

Esse experimento, reproduziu a clusterização do experimento 4, porém utilizando uma rede neural para realizar a separação das imagens. O resultado foi muito melhor.
Aqui foram testados o desempenho de 4 modelos (dois bem simples, ResNet e VGG16), sem utilizar o crop do bound-box e utilizando data augmentation. 

[loss exp6](assets/exp6_loss.png)

[acuracy exp6](assets/exp6_acc.png)

7. **Experimento 7**:

Nesse experimento, as redes mais simples foram treinadas em dois conjuntos. Um conjunto chamado toy_data7, uma versão melhorada do conjunto do experimento 6 e uma rodada com o conjunto completo do Covid-X.

[loss exp7](assets/exp7_loss.png)

[acuracy exp7](assets/exp7_acc.png)

7. **Experimento 7**:

# Análises

limitações

resultados da performance treino e validação
