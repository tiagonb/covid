# Trained models for Covid classification

## models cnd e cndd input

numpy array =  (n, 512, 512, 1)

## model resnet (imagenet weights with tranfer learning)

numpy array =  (n, 512, 512, 3)

## example (cnd model):

```
import numpy as np
from PIL import Image
from tensorflow.keras.models import load_model
from tensorflow.keras.preprocessing.image import img_to_array


image_path = "path/to/image"
image = Image.open(image_path).convert("L")
input_arr = img_to_array(image)
input_arr = np.array([input_arr])

model = load_model("cnd_da94eb.h5")
predictions = model.predict(input_arr)
```

## example (resnet model):

```
import numpy as np
from PIL import Image
from tensorflow.keras.models import load_model
from tensorflow.keras.preprocessing.image import img_to_array


image_path = "path/to/image"
image = Image.open(image_path).convert("L")
image = np.stack((image, image, image), axis=-1)
input_arr = img_to_array(image)
input_arr = np.array([input_arr])

model = load_model("resnet50_a2e0cc.h5")
predictions = model.predict(input_arr)
```


