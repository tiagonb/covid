#!/usr/bin/env python
# coding: utf-8

from keras import optimizers
from keras.preprocessing.image import ImageDataGenerator, load_img
from keras.applications.resnet50 import preprocess_input 
from keras.models import load_model
from sklearn.utils import class_weight
import mlflow.keras

from preprocessing.ImgaugDataGenerator import ImgaugDataGenerator

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import os, pathlib, math, argparse

import tensorflow as tf
gpus = tf.config.experimental.list_physical_devices('GPU')
if gpus:
  try:
    # Currently, memory growth needs to be the same across GPUs
    for gpu in gpus:
      tf.config.experimental.set_memory_growth(gpu, True)
    logical_gpus = tf.config.experimental.list_logical_devices('GPU')
    print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPUs")
  except RuntimeError as e:
    # Memory growth must be set before GPUs have been initialized
    print(e)
tf.test.gpu_device_name()


"""Parsing input"""

parser = argparse.ArgumentParser(description='Process covidnet evaluate input parameters.')
parser.add_argument("-d", "--dataset", type=str, default="", help="path to images")
parser.add_argument("-r", "--load-model", type=str, default="", help="load model from .h5 file")
parser.add_argument("-c", "--colormode", type=str, default="grayscale", help="colormode for input imagens: rgb=3 channnels, grayscale=1 channel")
args = vars(parser.parse_args())


data_path_str = args['dataset']
load_model_path = args['load_model']
colormode = args['colormode']
#data_path_str = '/misc/users/tiagonb/covid-net/dataset/NCP/88/1309'
#load_model_path = '/misc/users/tiagonb/covid-net/tools/experiments/mlruns/6/3728ffeddd1e44b8a8909421793a6d88/artifacts/model/data/model.h5'


"""Data Paths"""

data_path = pathlib.Path(data_path_str)

# keras trick - remove last dir and used it in flow_from_directory classes
last_dir = data_path.absolute().name
data_path = pathlib.Path(data_path.absolute().parent)


"""Neural Network"""

model = load_model(load_model_path)
model.summary()


"""Model Predictions (data_path)"""

if colormode == 'rgb':
    pred_datagen = ImageDataGenerator(preprocessing_function=preprocess_input)
else:
    pred_datagen = ImgaugDataGenerator(rescale=1./255, 
                                bboxes = [],
                                is_train = False)

pred_generator = pred_datagen.flow_from_directory(
        data_path,
        color_mode=colormode,
        target_size=(512, 512),
        batch_size=1,
        shuffle = False,
        classes=[last_dir],
        class_mode='categorical')

filepaths = pred_generator.filepaths
n_samples = len(filepaths)

predict = model.predict(pred_generator, steps = n_samples)


"""Post process predictions"""

predicted_class_indices = np.argmax(predict,axis=1)
max_prob = np.max(predict,axis=1)

# labels follow alphanumerical subdirectories order from train_generator data directory
labels = {'CP': 0, 'NCP': 1, 'Normal': 2}
labels_min = {'CP': 0, 'NC': 1, 'No': 2}
#labels = pred_generator.class_indices
labels = dict((v,k) for k,v in labels.items())
labels_min = dict((v,k) for k,v in labels_min.items())
predicted_class = [labels[k] for k in predicted_class_indices]

results = list(zip(filepaths, predict, predicted_class))

#save predictions in feather file for post analysis
saved_pred = pd.DataFrame({'model': [load_model_path]*n_samples,
                           'dataset': [data_path_str]*n_samples,
                           'files': filepaths,
                           'prob_CP': predict[:,0],
                           'prob_NCP': predict[:,1],
                           'prob_Normal': predict[:,2],
                           'pred_class': predicted_class})


"""Post process predictions"""

print("feather file modifications (rows, columns):")
print("saved_pred shape", saved_pred.shape)

p = pathlib.Path("./predictions.feather")
if p.is_file():
    data = pd.read_feather(p)
    print("loaded data shape", data.shape)
    data = data.drop(data[(data['model'] == load_model_path) & (data['dataset'] == data_path_str)].index)
    print("removed data shape", data.shape)
    data = pd.concat([data, saved_pred], ignore_index=True)
    print("concated data shape", data.shape)
    data.to_feather(p)
else:
    saved_pred.to_feather(p)

