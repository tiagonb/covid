# -*- coding: utf-8 -*-

#from tensorflow import keras
#import keras
#from tensorflow.keras import models
#from tensorflow.keras import layers
#from tensorflow.keras import optimizers
#from tensorflow.keras.preprocessing.image import ImageDataGenerator

#from keras import layers
#from keras import models
from keras import optimizers
from keras.preprocessing.image import ImageDataGenerator
from keras.applications.resnet50 import preprocess_input
from sklearn.utils import class_weight
from keras.models import load_model
import mlflow.keras

from preprocessing.ImgaugDataGenerator import ImgaugDataGenerator
from models.models import CatsnDogs, CatsnDogsDropout, ResNet50Custom, PaperNet, EarlyBifurcation, LaterBifurcation

import numpy as np
import os
import pathlib
import math
import argparse

import tensorflow as tf
gpus = tf.config.experimental.list_physical_devices('GPU')
if gpus:
  try:
    # Currently, memory growth needs to be the same across GPUs
    for gpu in gpus:
      tf.config.experimental.set_memory_growth(gpu, True)
    logical_gpus = tf.config.experimental.list_logical_devices('GPU')
    print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPUs")
  except RuntimeError as e:
    # Memory growth must be set before GPUs have been initialized
    print(e)
tf.test.gpu_device_name()


data_path_str = "../full_dataset"
#data_path_str = "../toy_data7_cluster2"

cnd="/misc/users/tiagonb/covid-net/tools/experiments/mlruns/6/da94eba6fec04c908d77f33ea7f1f405/artifacts/model/data/model.h5"
cndd="/misc/users/tiagonb/covid-net/tools/experiments/mlruns/6/3728ffeddd1e44b8a8909421793a6d88/artifacts/model/data/model.h5"
res50="/misc/users/tiagonb/covid-net/tools/experiments/mlruns/6/a2e0cca363864022bb95d29362abea83/artifacts/model/data/model.h5"

load_model_path = res50


"""Data Paths"""

data_path = pathlib.Path(data_path_str)

train_path = data_path / "train"
val_path = data_path / "val"
test_path = data_path / "test"


"""Neural Network"""

########################
# Create a MirroredStrategy for multiple GPU
#strategy = tf.distribute.MirroredStrategy()
#print("Number of devices: {}".format(strategy.num_replicas_in_sync))

# Open a strategy scope. Build and compile model inside this scope
#with strategy.scope():
########################

"""Neural Network"""

model = load_model(load_model_path)
model.summary()

######  to disable seed, set seed=None
seed = 1
ImgaugDataGenerator.set_ia_seed(seed)


"""Model Evaluation (test data)"""

# Evaluate the model on the test data using `evaluate`
#test_datagen = ImgaugDataGenerator(rescale=1./255, bboxes = {}, is_train = False)
test_datagen = ImgaugDataGenerator(preprocessing_function=preprocess_input, is_train = False)

test_generator = test_datagen.flow_from_directory(
        test_path,
        #color_mode="grayscale",
        color_mode="rgb",
        target_size=(512, 512),
        batch_size=20,
        seed=seed,
        class_mode='categorical')

print("Evaluate on test data")
#results = model.evaluate(test_generator, batch_size=BATCH_SIZE)
results = model.evaluate(test_generator)
print("test loss, test acc:", results)

