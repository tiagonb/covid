# -*- coding: utf-8 -*-

#from tensorflow import keras
#import keras
#from tensorflow.keras import models
#from tensorflow.keras import layers
#from tensorflow.keras import optimizers
#from tensorflow.keras.preprocessing.image import ImageDataGenerator

#from keras import layers
#from keras import models
from keras import optimizers
from keras.preprocessing.image import ImageDataGenerator
from sklearn.utils import class_weight
import mlflow.keras

from preprocessing.ImgaugDataGenerator import ImgaugDataGenerator
from models.models import CatsnDogs, CatsnDogsDropout, ResNet50Custom, PaperNet, EarlyBifurcation, LaterBifurcation
from utils.figures import plot_trainval_metrics, show_predictions

import numpy as np
import os
import pathlib
import math
import argparse

import tensorflow as tf
gpus = tf.config.experimental.list_physical_devices('GPU')
if gpus:
  try:
    # Currently, memory growth needs to be the same across GPUs
    for gpu in gpus:
      tf.config.experimental.set_memory_growth(gpu, True)
    logical_gpus = tf.config.experimental.list_logical_devices('GPU')
    print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPUs")
  except RuntimeError as e:
    # Memory growth must be set before GPUs have been initialized
    print(e)
tf.test.gpu_device_name()

# fname = os.path.sep.join([args["weights"], "weights-{epoch:03d}-{val_loss:.4f}.hdf5"])
parser = argparse.ArgumentParser(description='Process covidnet train input parameters.')
parser.add_argument("-d", "--dataset", type=str, default="../toy_data3_double", help="path to input dataset")
parser.add_argument("-r", "--model-name", type=str, default="CatsnDogs", help="model from .conv to run experiments")
parser.add_argument("-o", "--optimizer", type=str, default="Adam", help="choose optimizer (Adam, RMS, SGD)")
parser.add_argument("-x", "--experiment-name", type=str, default="exp4", help="choose name for experiment")
parser.add_argument("-n", "--run-name", type=str, default="", help="choose name for this run")
parser.add_argument("-l", "--lr", type=float, default=0.001, help="learning rate")
parser.add_argument("-e", "--epochs", type=int, default=30, help="number of epochs")
parser.add_argument("-b", "--batch-size", type=int, default=20, help="size of batch")
parser.add_argument("--no-bbox", default=False, action="store_true" , help="dont apply bound-box crop")
parser.add_argument("--no-augmentation", default=False, action="store_true" , help="dont apply data augmentation")
parser.add_argument("-t", "--tags", type=str, default="", help="set tags in training runs")
args = vars(parser.parse_args())


long_name = "{}[{}](opt={},lr={},e={},b={},bbox={},aug={},d={})".format(args['experiment_name'], args['model_name'], 
                                                     args['optimizer'], 
                                                     args['lr'], args['epochs'], 
                                                     args['batch_size'], 
                                                     not args['no_bbox'], 
                                                     not args['no_augmentation'], 
                                                     args['dataset'].split('/')[-1])

run_name = args['run_name']

data_path_str = args['dataset']
txt_path_str = args['dataset']


"""Load data for bounding-boxes"""

txt_path = pathlib.Path(txt_path_str)
#bound box dictionary with filename em bbox cordinates
p_train = txt_path/'train.txt'
p_val = txt_path/'val.txt'
p_test = txt_path/'test.txt'
all_txt = p_train.read_text() + p_val.read_text() + p_test.read_text()

bboxes_dict = {}
if not args['no_bbox']:
    print("Training with bound-box crop")
    for i in all_txt.split('\n'):
        if i:
            f = i.split()[0]
            bboxes_dict[f]=i


"""Data Paths"""

data_path = pathlib.Path(data_path_str)

train_path = data_path / "train"
val_path = data_path / "val"
test_path = data_path / "test"


"""Neural Network"""

########################
# Create a MirroredStrategy for multiple GPU
#strategy = tf.distribute.MirroredStrategy()
#print("Number of devices: {}".format(strategy.num_replicas_in_sync))

# Open a strategy scope. Build and compile model inside this scope
#with strategy.scope():
########################

if args['model_name'] == "CatsnDogsDropout":
    model = CatsnDogsDropout.build(width=512, height=512, depth=1, classes=3)
elif args['model_name'] == "CatsnDogs":
    model = CatsnDogs.build(width=512, height=512, depth=1, classes=2)
elif args['model_name'] == "ResNet50Custom":
    model = ResNet50Custom.build(width=512, height=512, depth=1, classes=3)
elif args['model_name'] == "PaperNet":
    model = PaperNet.build(width=512, height=512, depth=1, classes=3)
elif args['model_name'] == "EarlyBifurcation":
    model = EarlyBifurcation.build(width=512, height=512, depth=1, classes=3)
elif args['model_name'] == "LaterBifurcation":
    model = LaterBifurcation.build(width=512, height=512, depth=1, classes=3)
else:
    raise AssertionError("The --model-name command line argument should be a valid model")

model.summary()

if args['optimizer'] == "SGD":
    optimizer = optimizers.SGD(lr=args['lr'])
elif args['optimizer'] == "RMS":
    optimizer = optimizers.RMSprop(lr=args['lr'])
elif args['optimizer'] == "Adam":
    optimizer = optimizers.Adam(lr=args['lr'])
else:
    raise AssertionError("The --optimizer command line argument should be a valid optimizer")

model.compile(loss='categorical_crossentropy',
              optimizer=optimizer,
              metrics=['acc'])


#os.chdir("/content/gdrive/My Drive/covidnet/")
#model.save_weights('model_weights_dogs_cats.h5')


"""Data Preprocessing (train and validation)"""

BATCH_SIZE = args['batch_size']
EPOCHS = args['epochs']

######  to disable seed, set seed=None
seed = 1
ImgaugDataGenerator.set_ia_seed(seed)

if args['no_augmentation']:
    print("Training without data augmentation")
    train_datagen = ImgaugDataGenerator(rescale=1./255, bboxes = bboxes_dict, is_train = False)
else:
    print("Training with data augmentation")
    train_datagen = ImgaugDataGenerator(rescale=1./255, bboxes = bboxes_dict, is_train = True)

val_datagen = ImgaugDataGenerator(rescale=1./255, bboxes = bboxes_dict, is_train = False)

train_generator = train_datagen.flow_from_directory(
        train_path,
        color_mode="grayscale",
        target_size=(512, 512),
        batch_size=BATCH_SIZE,
        seed=seed,
        class_mode='categorical')

y_train_cls = train_generator.classes
class_weights = class_weight.compute_class_weight('balanced', np.unique(y_train_cls), y_train_cls)
class_weights = dict(zip(range(len(class_weights)), class_weights))

validation_generator = val_datagen.flow_from_directory(
        val_path,
        color_mode="grayscale",
        target_size=(512, 512),
        batch_size=BATCH_SIZE,
        seed=seed,
        class_mode='categorical')

n_samples_train = train_generator.samples
n_samples_validation = validation_generator.samples

steps_per_epoch_train = int(math.ceil(1. * n_samples_train / BATCH_SIZE))
steps_per_epoch_validation = int(math.ceil(1. * n_samples_validation / BATCH_SIZE))

print("run name: ", run_name)
print("class weights: ", class_weights)
print("training_size:{}, batch_size:{}, epochs:{}, steps_per_epoch:{}".format(
    n_samples_train, BATCH_SIZE, EPOCHS, steps_per_epoch_train))
print("validation_size:{}, batch_size:{}, epochs:{}, steps_per_epoch:{}".format(
    n_samples_validation, BATCH_SIZE, EPOCHS, steps_per_epoch_validation))


try:
    experiment_id = mlflow.create_experiment(name=args['experiment_name'])
except:
    experiment_id = mlflow.get_experiment_by_name(name=args['experiment_name']).experiment_id


"""Running model (train with validation)"""

with mlflow.start_run(experiment_id=experiment_id, run_name=run_name) as run:
    #mlflow.tensorflow.autolog(every_n_iter=1)
    mlflow.keras.autolog()

    history = model.fit(
          train_generator,
          steps_per_epoch=steps_per_epoch_train,
          epochs=EPOCHS,
          validation_data=validation_generator,
          validation_steps=steps_per_epoch_validation,
          class_weight=class_weights)


    """Plotting results"""

    acc = history.history['acc']
    val_acc = history.history['val_acc']
    loss = history.history['loss']
    val_loss = history.history['val_loss']

    figname = plot_trainval_metrics(acc, val_acc, 'accuracy', long_name)
    mlflow.log_artifact(figname) 
    figname = plot_trainval_metrics(loss, val_loss, 'loss', long_name)
    mlflow.log_artifact(figname) 

    # set tags 
    if args['tags']:
        for tag in args['tags'].split(','):
            tag_k, tag_v = tag.split("=")
            mlflow.set_tag(tag_k, tag_v)


    """Model Evaluation (test data)"""

    # Evaluate the model on the test data using `evaluate`
    test_datagen = ImgaugDataGenerator(rescale=1./255, bboxes = bboxes_dict, is_train = False)
    test_generator = test_datagen.flow_from_directory(
            test_path,
            color_mode="grayscale",
            target_size=(512, 512),
            batch_size=BATCH_SIZE,
            seed=seed,
            class_mode='categorical')

    print("Evaluate on test data")
    #results = model.evaluate(test_generator, batch_size=BATCH_SIZE)
    results = model.evaluate(test_generator)
    print("test loss, test acc:", results)
    mlflow.log_metric("test_loss", results[0])
    mlflow.log_metric("test_acc", results[1])

    # os.chdir("/content/gdrive/My Drive/covidnet/")
    # model.save('keras_covid_test1.h5')


    """Model Predictions (test data)"""

    pred_datagen = ImgaugDataGenerator(rescale=1./255, 
                                    bboxes = bboxes_dict,
                                    is_train = False)

    pred_generator = test_datagen.flow_from_directory(
            test_path,
            color_mode="grayscale",
            target_size=(512, 512),
            batch_size=1,
            shuffle = False,
            class_mode='categorical')

    filepaths = pred_generator.filepaths
    n_samples = len(filepaths)

    predict = model.predict(pred_generator, steps = n_samples)

    fig_classes = pred_generator.classes
    predicted_class_indices = np.argmax(predict,axis=1)
    max_prob = np.max(predict,axis=1)

    labels = pred_generator.class_indices
    labels = dict((v,k) for k,v in labels.items())
    predictions = [labels[k] for k in predicted_class_indices]
    fig_classes = [labels[k] for k in fig_classes]

    results = list(zip(filepaths, fig_classes, predictions, max_prob))

    results_wrongs = [i for i in results if i[1] != i[2]]
    results_wrongs.sort(key = lambda x: x[3])
    results_wrongs = results_wrongs[:18] + results_wrongs[-18:]

    results_successes = [i for i in results if i[1] == i[2]]
    results_successes.sort(key = lambda x: x[3])
    results_successes = results_successes[:18] + results_successes[-18:]

    figname = show_predictions(results_wrongs, 'wrongs', long_name, bboxes_dict, crop=False)
    mlflow.log_artifact(figname) 
    figname = show_predictions(results_wrongs, 'wrongs_cropped', long_name, bboxes_dict, crop=True)
    mlflow.log_artifact(figname) 
    figname = show_predictions(results_successes, 'successes', long_name, bboxes_dict, crop=False)
    mlflow.log_artifact(figname) 
    figname = show_predictions(results_successes, 'successes_cropped', long_name, bboxes_dict, crop=True)
    mlflow.log_artifact(figname) 
