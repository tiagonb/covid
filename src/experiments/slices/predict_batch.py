#!/usr/bin/env python
# coding: utf-8

from keras import optimizers
from keras.preprocessing.image import ImageDataGenerator, load_img
from keras.applications.resnet50 import preprocess_input 
from keras.models import load_model
from sklearn.utils import class_weight
import mlflow.keras

from preprocessing.ImgaugDataGenerator import ImgaugDataGenerator

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import os, pathlib, math, argparse


def get_all_exams_covidx_by_train_type(rootdir, train_type='train'):
    #loading predictions from feather file (processed in another notebook)
    p = pathlib.Path("./processed_df.feather")
    if p.is_file():
        processed_df = pd.read_feather(p)
    else:
        processed_df = pd.DataFrame()
        
    #aggregate all files and get type
    processed_agg = processed_df.groupby(['label', 'patient_id', 'scan_id']).agg({'type': 'max'})
    f = processed_agg.reset_index()

    dirs = []
    for p in pathlib.Path(rootdir).glob('**/*'):
        #just paths with 9 parts
        if p.is_dir() and len(p.parts) == 9:
            
            label, patient_id, scan_id = p.parts[-3], p.parts[-2], p.parts[-1]

            df_type =  f[(f['label'] == label) & 
                         (f['patient_id'] == int(patient_id)) & 
                         (f['scan_id'] == scan_id)]
            if df_type['type'].empty:
                continue
            if (df_type['type'].values)[0] == train_type:
                dirs.append(p)
    print("{} directories found for {}".format(len(dirs), train_type))
    dirs = [str(d) for d in dirs]
    return dirs


#use colormode="rgb" for resnet50 trained model
def make_predictions(data_paths, load_model_path, colormode="grayscale"):
    
    """Neural Network"""

    model = load_model(load_model_path)
    model.summary()

    results_df = pd.DataFrame()
    """Data Paths"""
    for i, data_path_str in enumerate(data_paths):
        data_path = pathlib.Path(data_path_str)
        print("starting - {}. {}".format(i, data_path))

        # keras trick - remove last dir and used it in flow_from_directory classes
        last_dir = data_path.absolute().name
        data_path = pathlib.Path(data_path.absolute().parent)


        """Model Predictions (data_path)"""

        if colormode == 'rgb':
            pred_datagen = ImageDataGenerator(preprocessing_function=preprocess_input)
        else:
            #pred_datagen = ImgaugDataGenerator(rescale=1./255, is_train = False)
            pred_datagen = ImageDataGenerator(rescale=1./255)

        pred_generator = pred_datagen.flow_from_directory(
                data_path,
                color_mode=colormode,
                target_size=(512, 512),
                batch_size=1,
                shuffle = False,
                classes=[last_dir],
                class_mode='categorical')

        filepaths = pred_generator.filepaths
        n_samples = len(filepaths)

        predict = model.predict(pred_generator, steps = n_samples)
        print("predicted - {}. number of predictions {}".format(i, len(predict)))


        """Post process predictions"""

        predicted_class_indices = np.argmax(predict,axis=1)
        max_prob = np.max(predict,axis=1)

        # labels follow alphanumerical subdirectories order from train_generator data directory
        labels = {'CP': 0, 'NCP': 1, 'Normal': 2}
        labels = dict((v,k) for k,v in labels.items())
        predicted_class = [labels[k] for k in predicted_class_indices]

        #save predictions in feather file for post analysis
        pred_df = pd.DataFrame({'model': [load_model_path]*n_samples,
                                   'dataset': [data_path_str]*n_samples,
                                   'files': filepaths,
                                   'prob_CP': predict[:,0],
                                   'prob_NCP': predict[:,1],
                                   'prob_Normal': predict[:,2],
                                   'pred_class': predicted_class})
        
        if not results_df.empty:
            results_df = pd.concat([results_df, pred_df], ignore_index=True)
        else:
            results_df = pred_df


    """Post process predictions"""

    print("feather file modifications (rows, columns):")
    print("results_df shape", results_df.shape)

    p = pathlib.Path("./predictions_training_set.feather")
    if p.is_file():
        data = pd.read_feather(p)
        print("loaded data shape", data.shape)
        data = pd.concat([data, results_df], ignore_index=True)
        print("concated data shape", data.shape)
        data.drop_duplicates(subset=['model','dataset','files'], keep='last', inplace=True)
        data.reset_index(drop=True, inplace=True)
        print("removed duplicates data shape", data.shape)
        data.to_feather(p)
    else:
        results_df.to_feather(p)

if __name__ == '__main__':
    import tensorflow as tf
    gpus = tf.config.experimental.list_physical_devices('GPU')
    if gpus:
      try:
        # Currently, memory growth needs to be the same across GPUs
        for gpu in gpus:
          tf.config.experimental.set_memory_growth(gpu, True)
        logical_gpus = tf.config.experimental.list_logical_devices('GPU')
        print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPUs")
      except RuntimeError as e:
        # Memory growth must be set before GPUs have been initialized
        print(e)
    tf.test.gpu_device_name()

    rootdir='/misc/users/tiagonb/covid-net/dataset'
    dirs = get_all_exams_covidx_by_train_type(rootdir, train_type='train')
    #resnet50         
    load_model_path="/misc/users/tiagonb/covid-net/tools/experiments/mlruns/6/a2e0cca363864022bb95d29362abea83/artifacts/model/data/model.h5"
    #cnd
    #load_model_path="/misc/users/tiagonb/covid-net/tools/experiments/mlruns/6/da94eba6fec04c908d77f33ea7f1f405/artifacts/model/data/model.h5"
    #cndd
    #load_model_path="/misc/users/tiagonb/covid-net/tools/experiments/mlruns/6/3728ffeddd1e44b8a8909421793a6d88/artifacts/model/data/model.h5"
    make_predictions(dirs, load_model_path, colormode="rgb")
