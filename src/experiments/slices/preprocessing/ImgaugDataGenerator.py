from .DirectoryIteratorWithFilename import DirectoryIteratorWithFilename
from keras.preprocessing.image import ImageDataGenerator
from imgaug.augmentables.bbs import BoundingBox, BoundingBoxesOnImage
import imgaug as ia
import imgaug.augmenters as iaa
import pathlib


"""#Classe ImgaugDataGenerator (utiliza a biblioteca Imgaug)"""

class ImgaugDataGenerator(ImageDataGenerator):


    def __init__(self, bboxes={}, is_train=True, 
                 resize_croped='original', **kwargs):
      super().__init__(**kwargs)
      self.bboxes = bboxes
      self.is_train = is_train
      self.resize_croped = resize_croped
    
    @staticmethod
    def set_ia_seed(value=None):
      if value is not None:
        ia.seed(value)

    def imgaug_transform(self, image, fname):
      h, w, ch = image.shape
      if not self.resize_croped:
        pass
      elif self.resize_croped == "original":
        res_y = h
        res_x = w
      else:
        res_y = self.resize_croped[0]
        res_x = self.resize_croped[1]

      if self.is_train:
        seq = iaa.Sequential([iaa.Affine(
            scale={"x": (0.8, 1.2), "y": (0.8, 1.2)},               # scale images to 80-120% of their size, individually per axis
            translate_percent={"x": (-0.1, 0.1), "y": (-0.1, 0.1)}, # translate by -20 to +20 percent (per axis)
            rotate=(-10, 10),                                       # rotate by -10 to +10 degrees
            shear=(-10, 10),                                         # shear by -10 to +10 degrees
            )])
      else:
        seq = iaa.Noop()
      
      fn = pathlib.Path(fname)
      if fn.name in self.bboxes:
        #build bound box from dictionary
        bbox = [int(x) for x in self.bboxes[fn.name].split(' ')[-4:]]
        bbs = BoundingBoxesOnImage([BoundingBox(x1=bbox[0], x2=bbox[2], 
                                                y1=bbox[1], y2=bbox[3])], 
                                   shape=image.shape)
        
        #applying transformations
        image_aug, bbs_aug = seq(image=image, bounding_boxes=bbs)

        # image with bboxes before and after augmentation
        #image_before = bbs.draw_on_image(image, size=2)
        #image_after = bbs_aug.draw_on_image(image_aug, size=2)

        extracted_before = bbs.bounding_boxes[0].extract_from_image(image)
        if self.resize_croped:
          extracted_before = ia.imresize_single_image(extracted_before, 
                                                            (res_y, res_x))
        
        extracted_after = bbs_aug.bounding_boxes[0].extract_from_image(image_aug)
        if self.resize_croped:
          extracted_after = ia.imresize_single_image(extracted_after,
                                                           (res_y, res_x))
        image_aug = extracted_after

      else:
        #applying transformations
        image_aug = seq(image=image)
      
      return image_aug


    def flow_from_directory(self,
                            directory,
                            target_size=(256, 256),
                            color_mode='rgb',
                            classes=None,
                            class_mode='categorical',
                            batch_size=32,
                            shuffle=True,
                            seed=None,
                            save_to_dir=None,
                            save_prefix='',
                            save_format='png',
                            follow_links=False,
                            subset=None,
                            interpolation='nearest'):
        
        return DirectoryIteratorWithFilename(
            directory,
            self,
            target_size=target_size,
            color_mode=color_mode,
            classes=classes,
            class_mode=class_mode,
            data_format=self.data_format,
            batch_size=batch_size,
            shuffle=shuffle,
            seed=seed,
            save_to_dir=save_to_dir,
            save_prefix=save_prefix,
            save_format=save_format,
            follow_links=follow_links,
            subset=subset,
            interpolation=interpolation,
            dtype=self.dtype
        )
