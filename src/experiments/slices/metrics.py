import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as plticker
from matplotlib.patches import Patch
from sklearn.preprocessing import LabelEncoder
from sklearn.metrics import multilabel_confusion_matrix
import matplotlib
import pandas as pd
import pathlib
import math
from sklearn.metrics import accuracy_score, multilabel_confusion_matrix

from utils import model_shortname

#remove limits for rows and cols in dataframe listing
pd.set_option('display.max_colwidth', None)
pd.set_option('display.max_rows', None)


def classify_valid_threshold(df, threshold = 0.5, lung_part = None):
    if lung_part is not None:
        df = df[df['pred_lungclass'] == lung_part]
    new_df = df.sort_values(by=['files'], ascending=True).reset_index(drop=True)
    values = new_df['prob_NCP']
    slices = values.gt(threshold)
    if slices.any():
        return 'NCP'
    values = new_df['prob_CP']
    slices = values.gt(threshold)
    if slices.any():
        return 'CP'
    return 'Normal'


def classify_valid_block(df, threshold = 0.5, shift = 1, lung_part = None):
    if lung_part is not None:
        df = df[df['pred_lungclass'] == lung_part]
    new_df = df.sort_values(by=['files'], ascending=True).reset_index(drop=True)
    values = new_df['prob_NCP']
    values_cut = values.gt(threshold)
    if shift == 2:
        slices = values_cut & values_cut.shift(1) & values_cut.shift(-1) & values_cut.shift(2) & values_cut.shift(-2)
    elif shift == 3:
        slices = values_cut & values_cut.shift(1) & values_cut.shift(-1) & values_cut.shift(2) & values_cut.shift(-2) & values_cut.shift(3) & values_cut.shift(-3)
    else:
        slices = values_cut & values_cut.shift(1) & values_cut.shift(-1)
    slices.fillna(False, inplace=True)
    if slices.any():
        return 'NCP'
    values = new_df['prob_CP']
    values_cut = values.gt(threshold)
    if shift == 2:
        slices = values_cut & values_cut.shift(1) & values_cut.shift(-1) & values_cut.shift(2) & values_cut.shift(-2)
    elif shift == 3:
        slices = values_cut & values_cut.shift(1) & values_cut.shift(-1) & values_cut.shift(2) & values_cut.shift(-2) & values_cut.shift(3) & values_cut.shift(-3)
    else:
        slices = values_cut & values_cut.shift(1) & values_cut.shift(-1)
    slices.fillna(False, inplace=True)
    if slices.any():
        return 'CP'
    return 'Normal'


def classify_sum_percentage(df, threshold = 0.5, lung_part = None):
    if lung_part is not None:
        df = df[df['pred_lungclass'] == lung_part]
    sum_ncp = df['prob_NCP'].sum()
    sum_cp = df['prob_CP'].sum()
    sum_normal = df['prob_Normal'].sum()
    total = sum_ncp + sum_cp + sum_normal
    if (sum_ncp + sum_cp) / total > threshold:
        if sum_ncp >= sum_cp:
            return 'NCP'
        else:
            return 'CP'
    return 'Normal'
    

def classify_small_entropy_slices(df, threshold = 0.5, lung_part = None):
    if lung_part is not None:
        df = df[df['pred_lungclass'] == lung_part]
    new_df = df
    values = -(((new_df['prob_NCP']*np.log2(new_df['prob_NCP'])) + 
               (new_df['prob_CP']*np.log2(new_df['prob_CP'])) +
               (new_df['prob_Normal']*np.log2(new_df['prob_Normal']))) / np.log2(3))
    values_cut = values.lt(0.3)
    sum_ncp = (df['prob_NCP'] * values_cut.astype(int)).sum()
    sum_cp = (df['prob_CP'] * values_cut.astype(int)).sum()
    sum_normal = (df['prob_Normal'] * values_cut.astype(int)).sum()
    total = sum_ncp + sum_cp + sum_normal
    if sum_ncp / total > threshold:
        return 'NCP'
    if sum_cp / total > threshold:
        return 'CP'
    return 'Normal'


def metric_to_df(df, model_name, metric_name, prediction, observation, threshold = 0.5):
    result = [model_name, metric_name, threshold]
    acc_score = accuracy_score(observation, prediction)
    result.extend([acc_score])
    cnf_matrix = multilabel_confusion_matrix(observation, prediction, labels=['NCP', 'CP', 'Normal'])
    result.extend(cnf_matrix.ravel())
    
    series = pd. Series(result, index = df.columns)
    df = df.append(series, ignore_index=True)
    
    return df


#loading predictions from feather file (processed in another notebook)
p = pathlib.Path("./predictions_all_info.feather")
if p.is_file():
    df = pd.read_feather(p)
else:
    df = pd.DataFrame()
    
    
    
# dataframe to store metrics evaluation results
metrics_df = pd.DataFrame(columns = ["model_name", "metric", "threshold", "acc_score",
                                     "tn_ncp", "fp_ncp", "fn_ncp", "tp_ncp",
                                     "tn_cp", "fp_cp", "fn_cp", "tp_cp",
                                     "tn_normal", "fp_normal", "fn_normal", "tp_normal"
                                    ])  
models = df['model'].unique()
# models = ['/misc/users/tiagonb/covid-net/models/COVID-Net_CT-1_L',
#           '/misc/users/tiagonb/covid-net/models/COVID-Net_CT-1_S']
for model_name in models:
    new_df = df[(df['model'] == model_name)]
    for t in np.arange(0.0, 1.0, 0.01):
        counter = 0
        labels = []
        block_result1, block_result2, block_result3 = [], [], []
        threshold_result = []
        sum_percent_result = []
        small_entropy_result = []
        block_midllelung_result1, block_midllelung_result2, block_midllelung_result3 = [], [], []
        threshold_midllelung_result = []
        sum_percent_midllelung_result = []
        small_entropy_midllelung_result = []
        #loop throught all exams, each exam (g) contains # slices
        print("####### ", t)
        for i, ((d, l, s), g) in enumerate(new_df.groupby(['dataset', 'label', 'new_split_class'])):
            if s == 'train':
                counter += 1
                print("model:{}, counter:{}".format(model_name, counter))
                #print("dataset={}, label={}, split_class={}, dataframe={}".format(d, l, s, g.shape))
                #labels store exam truth condition (NCP, CP, Normal)
                labels.append(l)
                #metrics calculation for each exam
                res11 = classify_valid_block(g, threshold = t, shift = 1)
                res12 = classify_valid_block(g, threshold = t, shift = 2)
                res13 = classify_valid_block(g, threshold = t, shift = 3)
                res2 = classify_valid_threshold(g, threshold = t)
                res3 = classify_sum_percentage(g, threshold = t)
                res4 = classify_small_entropy_slices(g, threshold = t)
                res51 = classify_valid_block(g, threshold = t, shift = 1, lung_part = 'middle_lung')
                res52 = classify_valid_block(g, threshold = t, shift = 2, lung_part = 'middle_lung')
                res53 = classify_valid_block(g, threshold = t, shift = 3, lung_part = 'middle_lung')
                res6 = classify_valid_threshold(g, threshold = t, lung_part = 'middle_lung')
                res7 = classify_sum_percentage(g, threshold = t, lung_part = 'middle_lung')
                res8 = classify_small_entropy_slices(g, threshold = t, lung_part = 'middle_lung')
                #storing results 
                block_result1.append(res11)
                block_result2.append(res12)
                block_result3.append(res13)
                threshold_result.append(res2)
                sum_percent_result.append(res3)
                small_entropy_result.append(res4)
                block_midllelung_result1.append(res51)
                block_midllelung_result2.append(res52)
                block_midllelung_result3.append(res53)
                threshold_midllelung_result.append(res6)
                sum_percent_midllelung_result.append(res7)
                small_entropy_midllelung_result.append(res8)

        results = [('bloco (shift 1)', block_result1),
                   ('bloco (shift 2)', block_result2),
                   ('bloco (shift 3)', block_result3),
                   ('threshold', threshold_result),
                   ('soma', sum_percent_result),
                   ('soma entropia', small_entropy_result),
                   ('bloco (mid - sft 1)', block_midllelung_result1),
                   ('bloco (mid - sft 2)', block_midllelung_result2),
                   ('bloco (mid - sft 3)', block_midllelung_result3),
                   ('threshold (mid)', threshold_midllelung_result),
                   ('soma (mid)', sum_percent_midllelung_result),
                   ('soma entropia (mid)', small_entropy_midllelung_result)
               ]
        for name, result in results:
            metrics_df = metric_to_df(metrics_df, model_name, name, result, labels, threshold = t)
            
metrics_df.to_feather("./metrics_df.feather")