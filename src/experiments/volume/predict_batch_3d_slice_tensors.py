#!/usr/bin/env python
# coding: utf-8

from keras import optimizers
from keras.preprocessing.image import ImageDataGenerator, load_img
from keras.applications.resnet50 import preprocess_input 
from keras.models import load_model
from sklearn.utils import class_weight
import mlflow.keras

from preprocessing.ImgaugDataGenerator import ImgaugDataGenerator

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import os, pathlib, math, argparse

from scipy import ndimage
import nibabel as nib



def read_nifti_file(filepath):
    """Read and load volume"""
    # Read file
    scan = nib.load(filepath)
    # Get raw data
    scan = scan.get_fdata()
    return scan


def resize_volume(img):
    """Resize across z-axis"""
    # Set the desired depth
    desired_depth = 64
    desired_width = 256
    desired_height = 256
    # Get current depth
    current_depth = img.shape[-1]
    current_width = img.shape[0]
    current_height = img.shape[1]
    # Compute depth factor
    depth = current_depth / desired_depth
    width = current_width / desired_width
    height = current_height / desired_height
    depth_factor = 1 / depth
    width_factor = 1 / width
    height_factor = 1 / height
    # Rotate
    img = ndimage.rotate(img, 90, reshape=False)
    # Resize across z-axis
    img = ndimage.zoom(img, (width_factor, height_factor, depth_factor), order=1)
    return img


def process_scan(path):
    """Read and resize volume"""
    # Read scan
    volume = read_nifti_file(path)
    # Normalize
    #volume = normalize(volume)
    volume = volume * (1.0/255.0)
    volume = volume.astype("float32")
    # Resize width, height and depth
    volume = resize_volume(volume)
    return volume


#use colormode="rgb" for resnet50 trained model
def make_predictions(df, load_model_path, colormode="grayscale"):
    
    """Neural Network"""

    model = load_model(load_model_path)
    model.summary()

    results_df = pd.DataFrame()
    
    """Model Predictions (data_path)"""

    if colormode == 'rgb':
        pred_datagen = ImageDataGenerator(preprocessing_function=preprocess_input)
    else:
        pred_datagen = ImageDataGenerator(rescale=1./255)

    pred_generator=pred_datagen.flow_from_dataframe(
            dataframe=df,
            directory=None,
            x_col="files",
            y_col="label",
            batch_size=1,
            color_mode=colormode,
            shuffle=False,
            class_mode="binary",
            target_size=(512, 512))

    filepaths = pred_generator.filepaths
    n_samples = len(filepaths)

    predict = model.predict(pred_generator, steps = n_samples)

    """Post process predictions"""
    
    predicted_class = ["NCP" if x <=0.5 else "Normal" for x in predict]
    

    #save predictions in feather file for post analysis
    pred_df = pd.DataFrame({'model': [load_model_path]*n_samples,
                               'files': filepaths,
                               'prob_NCP': 1 - np.squeeze(predict),
                               'pred_class': predicted_class})

    if not results_df.empty:
        results_df = pd.concat([results_df, pred_df], ignore_index=True)
    else:
        results_df = pred_df


    """Post process predictions"""

    print("feather file modifications (rows, columns):")
    print("results_df shape", results_df.shape)

    p = pathlib.Path("./predictions_ordered_scans.feather")
    if p.is_file():
        data = pd.read_feather(p)
        print("loaded data shape", data.shape)
        data = pd.concat([data, results_df], ignore_index=True)
        print("concated data shape", data.shape)
        data.drop_duplicates(subset=['model','files'], keep='last', inplace=True)
        data.reset_index(drop=True, inplace=True)
        print("removed duplicates data shape", data.shape)
        data.to_feather(p)
    else:
        results_df.to_feather(p)

if __name__ == '__main__':
    import tensorflow as tf
    gpus = tf.config.experimental.list_physical_devices('GPU')
    if gpus:
      try:
        # Currently, memory growth needs to be the same across GPUs
        for gpu in gpus:
          tf.config.experimental.set_memory_growth(gpu, True)
        logical_gpus = tf.config.experimental.list_logical_devices('GPU')
        print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPUs")
      except RuntimeError as e:
        # Memory growth must be set before GPUs have been initialized
        print(e)
    tf.test.gpu_device_name()
    
    df = pd.read_feather("./predictions_all_info.feather")
    df_cut = df.loc[df['model'] == '/misc/users/user/covid-net/models/COVID-Net_CT-1_S']
    df_cut = df_cut[df_cut['lung_ordered'] == True]
    df_cut = df_cut.loc[df_cut['label'] != 'CP']
    df_cut = df_cut.sample(n=100, random_state=1)
    print("df_cut_shape", df_cut.shape)

    #cnd
    load_model_path="/misc/users/user/covid-net/tools/experiments/models_h5/00560fa_cnd.h5"
    make_predictions(df_cut, load_model_path, colormode="grayscale")

