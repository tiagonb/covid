# -*- coding: utf-8 -*-

"""
Script used to train CNN on slices (images) using tf.dataset as input to the trainig loop

Should be fastter then generators
"""

import tensorflow as tf
from tensorflow import keras

# import keras
# from tensorflow.keras import models
from tensorflow.keras import layers
from tensorflow.keras import optimizers

# from tensorflow.keras.preprocessing.image import ImageDataGenerator

# from keras import layers
# from keras import models
# from keras import optimizers
# from keras.preprocessing.image import ImageDataGenerator
from sklearn.utils import class_weight
import mlflow.keras

from models.models import CatsnDogs, CatsnDogsDropout

# from utils.figures import plot_trainval_metrics, show_predictions

import numpy as np
import cv2
import os
import pathlib
import math
import argparse


@tf.function
def tf_excluse_exterior(image):

    # From covidnet ct scan code
    def opencv_excluse_exterior(image):

        filt_image = cv2.GaussianBlur(image.astype(np.uint8), (5, 5), 0)
        thresh = cv2.threshold(
            filt_image[filt_image > 0], 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU
        )[0]
        bin_image = np.uint8(filt_image > thresh)

        # Find body contour
        contours, hierarchy = cv2.findContours(
            bin_image.astype(np.uint8), cv2.RETR_CCOMP, cv2.CHAIN_APPROX_SIMPLE
        )
        areas = [cv2.contourArea(cnt) for cnt in contours]
        body_idx = np.argmax(areas)
        body_cont = contours[body_idx].squeeze()

        # Exclude external regions by replacing with bg mean
        body_mask = np.zeros(image.shape, dtype=np.uint8)
        cv2.drawContours(body_mask, [body_cont], 0, 1, -1)
        body_mask = body_mask.astype(bool)
        bg_mask = (~body_mask) & (image > 0)
        bg_dark = bg_mask & ~bin_image  # exclude bright regions from mean
        # bg_mean = np.mean(image[bg_dark.astype(bool)])
        bg_mask_no_circle = (~body_mask) & (image >= 0)
        image[bg_mask_no_circle] = 0

        return image

    image_processed = tf.numpy_function(opencv_excluse_exterior, [image], tf.float32)
    image_processed.set_shape([512, 512, 1])

    return image_processed


def batch_excluse_exterior(batch_tensor, labels):

    new_images = tf.map_fn(lambda image: tf_excluse_exterior(image), batch_tensor)

    return new_images, labels


gpus = tf.config.experimental.list_physical_devices("GPU")
if gpus:
    try:
        # Currently, memory growth needs to be the same across GPUs
        for gpu in gpus:
            tf.config.experimental.set_memory_growth(gpu, True)
        logical_gpus = tf.config.experimental.list_logical_devices("GPU")
        print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPUs")
    except RuntimeError as e:
        # Memory growth must be set before GPUs have been initialized
        print(e)
tf.test.gpu_device_name()

# fname = os.path.sep.join([args["weights"], "weights-{epoch:03d}-{val_loss:.4f}.hdf5"])
parser = argparse.ArgumentParser(description="Process covidnet train input parameters.")
parser.add_argument(
    "-d",
    "--dataset",
    type=str,
    default="../toy_data3_double",
    help="path to input dataset",
)
parser.add_argument(
    "-r",
    "--model-name",
    type=str,
    default="CatsnDogs",
    help="model from .conv to run experiments",
)
parser.add_argument(
    "-o",
    "--optimizer",
    type=str,
    default="Adam",
    help="choose optimizer (Adam, RMS, SGD)",
)
parser.add_argument(
    "-x",
    "--experiment-name",
    type=str,
    default="exp4",
    help="choose name for experiment",
)
parser.add_argument(
    "-n", "--run-name", type=str, default="", help="choose name for this run"
)
parser.add_argument("-l", "--lr", type=float, default=0.001, help="learning rate")
parser.add_argument("-e", "--epochs", type=int, default=100, help="number of epochs")
parser.add_argument("-b", "--batch-size", type=int, default=20, help="size of batch")
parser.add_argument(
    "-t", "--tags", type=str, default="", help="set tags in training runs"
)
args = vars(parser.parse_args())


long_name = "{}[{}](opt={},lr={},e={},b={},d={})".format(
    args["experiment_name"],
    args["model_name"],
    args["optimizer"],
    args["lr"],
    args["epochs"],
    args["batch_size"],
    args["dataset"].split("/")[-1],
)

run_name = args["run_name"]
data_path_str = args["dataset"]


"""Data Paths"""

data_path = pathlib.Path(data_path_str)

train_path = data_path / "train"
val_path = data_path / "val"
test_path = data_path / "test"


"""Neural Network"""

########################
# Create a MirroredStrategy for multiple GPU
# strategy = tf.distribute.MirroredStrategy()
# print("Number of devices: {}".format(strategy.num_replicas_in_sync))

# Open a strategy scope. Build and compile model inside this scope
# with strategy.scope():
########################

if args["model_name"] == "CatsnDogsDropout":
    model = CatsnDogsDropout.build(width=512, height=512, depth=1, classes=2)
elif args["model_name"] == "CatsnDogs":
    model = CatsnDogs.build(width=512, height=512, depth=1, classes=2)
elif args["model_name"] == "ResNet50Custom":
    model = ResNet50Custom.build(width=512, height=512, depth=1, classes=3)
else:
    raise AssertionError(
        "The --model-name command line argument should be a valid model"
    )

model.summary()

if args["optimizer"] == "SGD":
    optimizer = optimizers.SGD(lr=args["lr"])
elif args["optimizer"] == "RMS":
    optimizer = optimizers.RMSprop(lr=args["lr"])
elif args["optimizer"] == "Adam":
    optimizer = optimizers.Adam(lr=args["lr"])
else:
    raise AssertionError(
        "The --optimizer command line argument should be a valid optimizer"
    )

model.compile(loss="binary_crossentropy", optimizer=optimizer, metrics=["acc"])


# os.chdir("/content/gdrive/My Drive/covidnet/")
# model.save_weights('model_weights_dogs_cats.h5')


"""Data Preprocessing (train and validation)"""

BATCH_SIZE = args["batch_size"]
EPOCHS = args["epochs"]

train_ds = tf.keras.preprocessing.image_dataset_from_directory(
    train_path,
    labels="inferred",
    color_mode="grayscale",
    image_size=(512, 512),
    batch_size=BATCH_SIZE,
)

val_ds = tf.keras.preprocessing.image_dataset_from_directory(
    val_path,
    labels="inferred",
    color_mode="grayscale",
    image_size=(512, 512),
    batch_size=BATCH_SIZE,
)

y_train_cls = np.array([x[1].numpy() for x in list(train_ds)])
y_train_cls = np.concatenate(y_train_cls).ravel().tolist()
y_val_cls = np.array([x[1].numpy() for x in list(val_ds)])
y_val_cls = np.concatenate(y_val_cls).ravel().tolist()
class_weights = class_weight.compute_class_weight(
    "balanced", np.unique(y_train_cls), y_train_cls
)
class_weights = dict(zip(range(len(class_weights)), class_weights))

rescaling = keras.Sequential([layers.experimental.preprocessing.Rescaling(1.0 / 255)])

data_augmentation = keras.Sequential(
    [
        layers.experimental.preprocessing.RandomZoom(
            height_factor=(-0.2, 0.2),
            width_factor=(-0.2, 0.2),
            fill_mode="constant",
            fill_value=0.0,
        ),
        layers.experimental.preprocessing.RandomTranslation(
            height_factor=(-0.1, 0.1),
            width_factor=(-0.1, 0.1),
            fill_mode="constant",
            fill_value=0.0,
        ),
        layers.experimental.preprocessing.RandomRotation(
            factor=(-0.1, 0.1), fill_mode="constant", fill_value=0.0
        ),
    ]
)

train_ds = train_ds.shuffle(len(y_train_cls)).map(batch_excluse_exterior)
train_ds = train_ds.map(lambda x, y: (rescaling(x, training=True), y))
train_ds = train_ds.map(lambda x, y: (data_augmentation(x, training=True), y))

val_ds = val_ds.shuffle(len(y_val_cls)).map(batch_excluse_exterior)
val_ds = val_ds.map(lambda x, y: (rescaling(x, training=True), y))

train_ds = train_ds.prefetch(buffer_size=40)
val_ds = val_ds.prefetch(buffer_size=40)


try:
    experiment_id = mlflow.create_experiment(name=args["experiment_name"])
except:
    experiment_id = mlflow.get_experiment_by_name(
        name=args["experiment_name"]
    ).experiment_id


"""Running model (train with validation)"""

with mlflow.start_run(experiment_id=experiment_id, run_name=run_name) as run:
    # mlflow.tensorflow.autolog(every_n_iter=1)
    mlflow.keras.autolog()

    model.fit(
        train_ds, epochs=EPOCHS, class_weight=class_weights, validation_data=val_ds
    )

    """Plotting results"""

    acc = history.history["acc"]
    val_acc = history.history["val_acc"]
    loss = history.history["loss"]
    val_loss = history.history["val_loss"]

    #     figname = plot_trainval_metrics(acc, val_acc, 'accuracy', long_name)
    #     mlflow.log_artifact(figname)
    #     figname = plot_trainval_metrics(loss, val_loss, 'loss', long_name)
    #     mlflow.log_artifact(figname)

    # set tags
    if args["tags"]:
        for tag in args["tags"].split(","):
            tag_k, tag_v = tag.split("=")
            mlflow.set_tag(tag_k, tag_v)
