import matplotlib.pyplot as plt
from imgaug.augmentables.bbs import BoundingBox, BoundingBoxesOnImage
import imageio
import imgaug as ia
import imgaug.augmenters as iaa
import numpy as np


def plot_trainval_metrics(train, val, metric, run_name):
    plt.figure()
    epochs = range(len(train))
    plt.plot(epochs, train, "bo", label="Training {}".format(metric))
    plt.plot(epochs, val, "b", label="Validation {}".format(metric))
    plt.title("Training and validation {}".format(metric))
    plt.legend()
    figname = "./png/{}-trainval_{}.png".format(run_name, metric)
    plt.savefig(figname)

    return figname


def show_predictions(results, title, run_name, bboxes_dict={}, crop=False):
    plt.figure(figsize=(16, 16))
    for i, (image_path, cls, pred, prob) in enumerate(results[:36]):
        filename = image_path.split("/")[-1]
        plt.subplot(6, 6, i + 1, xticks=[], yticks=[])
        image = imageio.imread(image_path)
        image = np.stack((image,) * 3, axis=-1)
        if filename in bboxes_dict:
            seq = iaa.Noop()
            bbox = [int(x) for x in bboxes_dict[filename].split(" ")[-4:]]
            bbs = BoundingBoxesOnImage(
                [BoundingBox(x1=bbox[0], x2=bbox[2], y1=bbox[1], y2=bbox[2])],
                shape=image.shape,
            )
            if crop:
                image, bbs = seq(image=image, bounding_boxes=bbs)
                image = bbs.bounding_boxes[0].extract_from_image(image)
                image = ia.imresize_single_image(image, (512, 512))
            else:
                image = bbs.draw_on_image(image, size=4)

        plt.imshow(image)
        c = "red" if pred != cls else "green"
        plt.xlabel("{}".format(filename))
        plt.title("{} ({:04.2f})".format(pred, prob), color=c)
    plt.tight_layout()
    figname = "./png/{}-panel_{}.png".format(run_name, title)
    plt.savefig(figname)

    return figname
