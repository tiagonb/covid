#!/usr/bin/env python
# coding: utf-8

"""
Script to train CNN on covidX dataset converted to nifti files

Use to compare results between volume and slices (grouped as volume)
"""
# This notebook run a 3D convnet for CT images agregated as volumetric data.


import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
import mlflow.tensorflow

import os
import math
import zipfile
import pandas as pd
import numpy as np
from scipy import ndimage
import nibabel as nib
from sklearn.utils import class_weight

import argparse


import cv2
from PIL import Image
import random
from scipy import ndimage


def read_nifti_file(filepath):
    """Read and load volume"""
    # Read file
    scan = nib.load(filepath)
    # Get raw data
    scan = scan.get_fdata()
    return scan


def resize_volume(img):
    """Resize across z-axis"""
    # Set the desired depth
    desired_depth = 64
    desired_width = 256
    desired_height = 256
    # Get current depth
    current_depth = img.shape[-1]
    current_width = img.shape[0]
    current_height = img.shape[1]
    # Compute depth factor
    depth = current_depth / desired_depth
    width = current_width / desired_width
    height = current_height / desired_height
    depth_factor = 1 / depth
    width_factor = 1 / width
    height_factor = 1 / height
    # Rotate
    img = ndimage.rotate(img, 90, reshape=False)
    # Resize across z-axis
    img = ndimage.zoom(img, (width_factor, height_factor, depth_factor), order=1)
    return img


def process_scan(path):
    """Read and resize volume"""
    # Read scan
    volume = read_nifti_file(path)
    # Normalize
    # volume = normalize(volume)
    volume = volume * (1.0 / 255.0)
    volume = volume.astype("float32")
    volume = extract_exterior_nifti(volume)
    # Resize width, height and depth
    volume = resize_volume(volume)
    return volume


def excluse_exterior(image):
    # Create initial binary image
    filt_image = cv2.GaussianBlur(image, (5, 5), 0)
    thresh = cv2.threshold(
        filt_image[filt_image > 0], 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU
    )[0]
    bin_image = np.uint8(filt_image > thresh)

    # Find body contour
    contours, hierarchy = cv2.findContours(
        bin_image.astype(np.uint8), cv2.RETR_CCOMP, cv2.CHAIN_APPROX_SIMPLE
    )[-2:]
    areas = [cv2.contourArea(cnt) for cnt in contours]
    body_idx = np.argmax(areas)
    body_cont = contours[body_idx].squeeze()

    # Exclude external regions by replacing with bg mean
    body_mask = np.zeros(image.shape, dtype=np.uint8)
    cv2.drawContours(body_mask, [body_cont], 0, 1, -1)
    body_mask = body_mask.astype(bool)
    bg_mask = (~body_mask) & (image > 0)
    bg_dark = bg_mask & ~bin_image  # exclude bright regions from mean
    bg_mean = np.mean(image[bg_dark.astype(bool)])
    bg_mask_no_circle = (~body_mask) & (image >= 0)
    #     image[bg_mask_no_circle] = bg_mean

    return bg_mask_no_circle, bg_mean


def extract_exterior_nifti(volume):
    """Receive volume with values between 0 and 1"""
    i, j, k = volume.shape
    for kk in range(k):
        a_slice = volume[:, :, kk]
        # reconvert to grayscale (0-255)
        image = 255 * a_slice
        image = np.uint8(image)
        image = Image.fromarray(image, "L")
        bg_mask_no_circle, bg_mean = excluse_exterior(np.uint8(image))
        # assign 0 (black) to background
        a_slice[bg_mask_no_circle] = 0
        volume[:, :, kk] = a_slice
        # return volume between 0-1

    return volume


@tf.function
def rotate(volume):
    def aff_random_transf(volume):
        w, h, d = volume.shape
        # scaling (scale images to 80-120% of their size, individually per axis)
        sx, sy = random.uniform(0.9, 1.1), random.uniform(0.9, 1.1)
        # image pixels center
        cx, cy = w / 2.0, h / 2.0
        mat_scale_centered = np.array(
            [[sx, 0, (1 - sx) * cx], [0, sy, (1 - sy) * cy], [0, 0, 1]]
        )
        # rotation (range by -10 to +10 degrees)
        theta = random.uniform(-10, 10)
        # shear (by -10 to +10 degrees)
        shear = random.uniform(-10, 10)
        # translation by -10 to +10 percent (per axis)
        dx, dy = random.uniform(-(w * 0.05), w * 0.05), random.uniform(
            -(h * 0.05), h * 0.05
        )
        # applying transformations
        volume = tf.keras.preprocessing.image.apply_affine_transform(
            volume,
            theta=theta,
            tx=dx,
            ty=dy,
            shear=shear,
            zx=sx,
            zy=sx,
            fill_mode="wrap",
        )
        volume[volume < 0] = 0
        volume[volume > 1] = 1
        return volume

    """Rotate the volume by a few degrees"""

    def scipy_rotate(volume):
        # define some rotation angles
        angles = [-20, -10, -5, 5, 10, 20]
        # pick angles at random
        angle = random.choice(angles)
        # rotate volume
        volume = ndimage.rotate(volume, angle, reshape=False)
        volume[volume < 0] = 0
        volume[volume > 1] = 1
        return volume

    # augmented_volume = tf.numpy_function(scipy_rotate, [volume], tf.float32)
    augmented_volume = tf.numpy_function(aff_random_transf, [volume], tf.float32)
    return augmented_volume


def train_preprocessing(volume, label):
    """Process training data by rotating and adding a channel."""
    # Rotate volume
    volume = rotate(volume)
    volume.set_shape([256, 256, 64])
    volume = tf.expand_dims(volume, axis=3)
    # (samples, height, width, depth, 1)
    # volume.set_shape([2, 128, 128, 64, 1])
    print("volume ", tf.shape(volume))
    return volume, label


def validation_preprocessing(volume, label):
    """Process validation data by only adding a channel."""
    volume.set_shape([256, 256, 64])
    volume = tf.expand_dims(volume, axis=3)
    # volume = tf.reshape(volume, shape=(in, input_height, input_channel))
    print("volume ", tf.shape(volume))
    return volume, label


# # model definition

# In[5]:


def get_model(width=256, height=256, depth=64):
    """Build a 3D convolutional neural network model."""

    inputs = keras.Input((width, height, depth, 1))

    x = layers.Conv3D(filters=64, kernel_size=3, activation="relu")(inputs)
    x = layers.MaxPool3D(pool_size=2)(x)
    # x = layers.BatchNormalization()(x)

    x = layers.Conv3D(filters=64, kernel_size=3, activation="relu")(x)
    x = layers.MaxPool3D(pool_size=2)(x)
    # x = layers.BatchNormalization()(x)

    x = layers.Conv3D(filters=128, kernel_size=3, activation="relu")(x)
    x = layers.MaxPool3D(pool_size=2)(x)
    # x = layers.BatchNormalization()(x)

    x = layers.Conv3D(filters=256, kernel_size=3, activation="relu")(x)
    x = layers.MaxPool3D(pool_size=2)(x)
    # x = layers.BatchNormalization()(x)

    x = layers.GlobalAveragePooling3D()(x)
    x = layers.Dense(units=512, activation="relu")(x)
    x = layers.Dropout(0.3)(x)

    outputs = layers.Dense(units=1, activation="sigmoid")(x)

    # Define the model.
    model = keras.Model(inputs, outputs, name="3dcnn")
    return model


# In[6]:


gpus = tf.config.experimental.list_physical_devices("GPU")
if gpus:
    try:
        # Currently, memory growth needs to be the same across GPUs
        for gpu in gpus:
            tf.config.experimental.set_memory_growth(gpu, True)
        logical_gpus = tf.config.experimental.list_logical_devices("GPU")
        print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPUs")
    except RuntimeError as e:
        # Memory growth must be set before GPUs have been initialized
        print(e)
tf.test.gpu_device_name()

# fname = os.path.sep.join([args["weights"], "weights-{epoch:03d}-{val_loss:.4f}.hdf5"])
parser = argparse.ArgumentParser(description="Process covidnet train input parameters.")
parser.add_argument(
    "-x",
    "--experiment-name",
    type=str,
    default="exp4",
    help="choose name for experiment",
)
parser.add_argument(
    "-n", "--run-name", type=str, default="", help="choose name for this run"
)
parser.add_argument(
    "-t", "--tags", type=str, default="", help="set tags in training runs"
)
args = vars(parser.parse_args())

run_name = args["run_name"]


train_normal_scan_paths = [
    os.path.join(os.getcwd(), "covid_nifti/train/Normal", x)
    for x in os.listdir("covid_nifti/train/Normal")
]
val_normal_scan_paths = [
    os.path.join(os.getcwd(), "covid_nifti/val/Normal", x)
    for x in os.listdir("covid_nifti/val/Normal")
]
train_abnormal_scan_paths = [
    os.path.join(os.getcwd(), "covid_nifti/train/NCP", x)
    for x in os.listdir("covid_nifti/train/NCP")
]
val_abnormal_scan_paths = [
    os.path.join(os.getcwd(), "covid_nifti/val/NCP", x)
    for x in os.listdir("covid_nifti/val/NCP")
]

print(
    "CT scans with normal lung tissue: "
    + str(len(train_normal_scan_paths + val_normal_scan_paths))
)
print(
    "CT scans with abnormal lung tissue: "
    + str(len(train_abnormal_scan_paths + val_abnormal_scan_paths))
)


# Read and process the scans.
# Each scan is resized across height, width, and depth and rescaled.
train_abnormal_scans = np.array(
    [process_scan(path) for path in train_abnormal_scan_paths]
)
train_normal_scans = np.array([process_scan(path) for path in train_normal_scan_paths])
val_abnormal_scans = np.array([process_scan(path) for path in val_abnormal_scan_paths])
val_normal_scans = np.array([process_scan(path) for path in val_normal_scan_paths])

# For the CT scans having presence of viral pneumonia
# assign 1, for the normal ones assign 0.
train_abnormal_labels = np.array([0 for _ in range(len(train_abnormal_scans))])
train_normal_labels = np.array([1 for _ in range(len(train_normal_scans))])
val_abnormal_labels = np.array([0 for _ in range(len(val_abnormal_scans))])
val_normal_labels = np.array([1 for _ in range(len(val_normal_scans))])

# Split data in the ratio 70-30 for training and validation.
x_train = np.concatenate((train_abnormal_scans, train_normal_scans), axis=0)
y_train = np.concatenate((train_abnormal_labels, train_normal_labels), axis=0)
x_val = np.concatenate((val_abnormal_scans, val_normal_scans), axis=0)
y_val = np.concatenate((val_abnormal_labels, val_normal_labels), axis=0)
print(
    "Number of samples in train and validation are %d and %d."
    % (x_train.shape[0], x_val.shape[0])
)

# classes balancing
y_train_cls = y_train
class_weights = class_weight.compute_class_weight(
    "balanced", np.unique(y_train_cls), y_train_cls
)
class_weights = dict(zip(range(len(class_weights)), class_weights))


# Define data loaders.
train_loader = tf.data.Dataset.from_tensor_slices((x_train, y_train))
validation_loader = tf.data.Dataset.from_tensor_slices((x_val, y_val))

batch_size = 1
# Augment the on the fly during training.
train_dataset = (
    train_loader.shuffle(len(x_train))
    .map(train_preprocessing)
    .batch(batch_size)
    .prefetch(2)
)
# Only rescale.
validation_dataset = (
    validation_loader.shuffle(len(x_val))
    .map(validation_preprocessing)
    .batch(batch_size)
    .prefetch(2)
)


# Build model.
model = get_model(width=256, height=256, depth=64)
model.summary()

# Compile model.
initial_learning_rate = 0.0001
lr_schedule = keras.optimizers.schedules.ExponentialDecay(
    initial_learning_rate, decay_steps=100000, decay_rate=0.96, staircase=True
)
model.compile(
    loss="binary_crossentropy",
    optimizer=keras.optimizers.Adam(learning_rate=lr_schedule),
    metrics=["acc"],
)

# Define callbacks.
checkpoint_cb = keras.callbacks.ModelCheckpoint(
    "3d_image_classification.h5", save_best_only=True
)
early_stopping_cb = keras.callbacks.EarlyStopping(monitor="val_acc", patience=15)


try:
    experiment_id = mlflow.create_experiment(name=args["experiment_name"])
except:
    experiment_id = mlflow.get_experiment_by_name(
        name=args["experiment_name"]
    ).experiment_id


"""Running model (train with validation)"""

with mlflow.start_run(experiment_id=experiment_id, run_name=run_name) as run:
    mlflow.tensorflow.autolog(every_n_iter=1)
    # mlflow.keras.autolog()
    # Train the model, doing validation at the end of each epoch
    epochs = 100
    history = model.fit(
        train_dataset,
        validation_data=validation_dataset,
        epochs=epochs,
        class_weight=class_weights,
        shuffle=True,
        verbose=2,
        callbacks=[early_stopping_cb],
    )

    acc = history.history["acc"]
    val_acc = history.history["val_acc"]
    loss = history.history["loss"]
    val_loss = history.history["val_loss"]

    # figname = plot_trainval_metrics(acc, val_acc, 'accuracy', '')
    # mlflow.log_artifact(figname)
    # figname = plot_trainval_metrics(loss, val_loss, 'loss', '')
    # mlflow.log_artifact(figname)

    # set tags
    if args["tags"]:
        for tag in args["tags"].split(","):
            tag_k, tag_v = tag.split("=")
            mlflow.set_tag(tag_k, tag_v)

    # print("Evaluate on test data")
    # results = model.evaluate(test_generator, batch_size=BATCH_SIZE)
    # results = model.evaluate(testing_generator)
    # print("test loss, test acc:", results)
    # mlflow.log_metric("test_loss", results[0])
    # mlflow.log_metric("test_acc", results[1])
