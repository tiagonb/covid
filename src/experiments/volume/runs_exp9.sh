EXPERIMENT_NAME="exp9"
DATASET="./full_dataset_lung_ordered"
RUN_PREFIX="00"

python train_tensor_from_slices.py --experiment-name $EXPERIMENT_NAME --run-name "$RUN_PREFIX-cnd-lungordered-ext-exclusion" --epochs 100 --batch-size 20 --model-name CatsnDogs --dataset $DATASET --tags "lung_ordered=yes,exclude_exterior=yes"

python train_tensor_from_slices.py --experiment-name $EXPERIMENT_NAME --run-name "$RUN_PREFIX-cndd-lungordered-ext-exclusion" --epochs 100 --batch-size 20 --model-name CatsnDogsDropout --dataset $DATASET --tags "lung_ordered=yes,exclude_exterior=yes"

python -u train_3dcovidnifti.py --experiment-name $EXPERIMENT_NAME --run-name "$RUN_PREFIX-cnd3D-256-covnifti-affaug-remove-exterior" --tags "lung_ordered=yes,exclude_exterior=yes" 

