#!/usr/bin/env python
# coding: utf-8

"""
Script to create plot sections of interpolated volumetric data
"""

import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
from tensorflow.keras.preprocessing.image import load_img, img_to_array
from tensorflow.keras.models import Sequential

import os
import math
import zipfile
import numpy as np
import pandas as pd
from scipy import ndimage
from pathlib import Path
import cv2
import random

from skimage.transform import warp, AffineTransform


# example from: https://keras.io/examples/vision/3D_image_classification/
def read_scan_path(filepath):
    """Read files and load volume"""
    # Read files
    files = sorted(list(Path(filepath).glob("**/*")))
    imgs = []
    #     print("### {} \nTotal of {} images.\n".format(filepath, len(files)))
    for i in files:
        img = cv2.imread(str(i), cv2.IMREAD_GRAYSCALE)
        # img = load_img(i, color_mode='grayscale')
        # x = img_to_array(np.squeeze(img))
        # img = remove_exterior(img)
        if hasattr(img, "close"):
            img.close()
        imgs.append(img)

    scan = np.squeeze(np.stack(imgs, axis=2))
    return scan


def affine_augmentation(volume):
    """
    Apply a sequence of affine transformations for data augmentation

    # Translation and then rotation
    coords_composite1 = R1 @ T1 @ coords
    # Rotation and then translation
    coords_composite2 = T1 @ R1 @ coords

    How the function is applied can be understood from right to left.
    """
    w, h, d = volume.shape
    # scaling (scale images to 80-120% of their size, individually per axis)
    sx, sy = random.uniform(0.9, 1.1), random.uniform(0.9, 1.1)
    # image pixels center
    cx, cy = w / 2.0, h / 2.0
    mat_scale_centered = np.array(
        [[sx, 0, (1 - sx) * cx], [0, sy, (1 - sy) * cy], [0, 0, 1]]
    )
    # rotation (range by -10 to +10 degrees)
    theta = random.uniform(-(np.pi / 18), np.pi / 18)
    mat_rotate = (
        np.array([[1, 0, w / 2], [0, 1, h / 2], [0, 0, 1]])
        @ np.array(
            [
                [np.cos(theta), np.sin(theta), 0],
                [np.sin(theta), -np.cos(theta), 0],
                [0, 0, 1],
            ]
        )
        @ np.array([[1, 0, -w / 2], [0, 1, -h / 2], [0, 0, 1]])
    )
    # shear (by -10 to +10 degrees)
    shear = random.uniform(0, 0.1)
    mat_shear = np.array([[1, shear, 0], [shear, 1, 0], [0, 0, 1]])
    # translation by -10 to +10 percent (per axis)
    dx, dy = random.uniform(-(w * 0.05), w * 0.05), random.uniform(
        -(h * 0.05), h * 0.05
    )
    mat_shift = np.array([[1, 0, dx], [0, 1, dy], [0, 0, 1]])
    # applying transformations
    mat_all = mat_scale_centered @ mat_shear @ mat_shift @ mat_rotate
    for k in range(volume.shape[-1]):
        volume[..., k] = ndimage.affine_transform(volume[..., k], mat_all, mode="wrap")

    return volume


def affine_augmentation2(volume):
    """Apply a sequence of affine transformations for data augmentation"""
    w, h, d = volume.shape
    # scaling (scale images to 80-120% of their size, individually per axis)
    sx, sy = random.uniform(0.8, 1.2), random.uniform(0.8, 1.2)
    # image pixels center
    cx, cy = w / 2.0, h / 2.0
    # rotation (range by -10 to +10 degrees)
    rotation = random.uniform(-(np.pi / 18), np.pi / 18)
    # shear (by -10 to +10 degrees)
    # shear = random.uniform(-(np.pi/18), np.pi/18)
    shear = random.uniform(0, 0.2)
    # translation by -10 to +10 percent (per axis)
    dx, dy = random.uniform(-(w * 0.05), w * 0.05), random.uniform(
        -(h * 0.05), h * 0.05
    )
    # transformations matrix
    transformation_mat = np.array(
        [
            [sx * np.cos(rotation), -sy * np.sin(rotation + shear), 0],
            [sx * np.sin(rotation), sy * np.cos(rotation + shear), 0],
            [0, 0, 1],
        ]
    )
    transformation_mat[0:2, 2] = (dx, dy)
    # print("sx={},sy={},rotation={},shear={},dx={},dy={}".format(sx,sy,rotation,shear,dx,dy))
    for k in range(volume.shape[-1]):
        volume[..., k] = ndimage.affine_transform(
            volume[..., k], transformation_mat, mode="wrap"
        )
    return volume


def affine_augmentation3(volume):
    """Apply a sequence of affine transformations for data augmentation (with skimage-kit)"""
    w, h, d = volume.shape
    # scaling (scale images to 80-120% of their size, individually per axis)
    sx, sy = random.uniform(0.9, 1.1), random.uniform(0.9, 1.1)
    # image pixels center
    cx, cy = w / 2.0, h / 2.0
    # rotation (range by -10 to +10 degrees)
    rotation = random.uniform(-(np.pi / 18), np.pi / 18)
    # shear (by -10 to +10 degrees)
    shear = random.uniform(0, 0.2)
    # translation by -10 to +10 percent (per axis)
    dx, dy = random.uniform(-(w * 0.05), w * 0.05), random.uniform(
        -(h * 0.05), h * 0.05
    )
    # scimage kit function
    tform = AffineTransform(
        scale=(sx, sy), rotation=rotation, shear=shear, translation=(dx, dy)
    )
    # print("sx={},sy={},theta={},lamb={},dx={},dy={}".format(sx,sy,theta,lamb,dx,dy))
    for k in range(volume.shape[-1]):
        volume[..., k] = warp(volume[..., k], tform.inverse)
    return volume


def affine_augmentation4(volume):
    """
    Apply a sequence of affine transformations for data augmentation

    # Translation and then rotation
    coords_composite1 = R1 @ T1 @ coords
    # Rotation and then translation
    coords_composite2 = T1 @ R1 @ coords

    How the function is applied can be understood from right to left.
    """
    w, h, d = volume.shape
    # scaling (scale images to 80-120% of their size, individually per axis)
    sx, sy = random.uniform(0.9, 1.1), random.uniform(0.9, 1.1)
    # image pixels center
    cx, cy = w / 2.0, h / 2.0
    mat_scale_centered = np.array(
        [[sx, 0, (1 - sx) * cx], [0, sy, (1 - sy) * cy], [0, 0, 1]]
    )
    # rotation (range by -10 to +10 degrees)
    theta = random.uniform(-10, 10)
    # shear (by -10 to +10 degrees)
    shear = random.uniform(-10, 10)
    # translation by -10 to +10 percent (per axis)
    dx, dy = random.uniform(-(w * 0.05), w * 0.05), random.uniform(
        -(h * 0.05), h * 0.05
    )
    # applying transformations
    volume = tf.keras.preprocessing.image.apply_affine_transform(
        volume, theta=theta, tx=dx, ty=dy, shear=shear, zx=sx, zy=sx, fill_mode="wrap"
    )
    return volume


def normalize(volume):
    """Normalize the volume"""
    min = -1450
    max = 50
    volume[volume < min] = min
    volume[volume > max] = max
    volume = (volume - min) / (max - min)
    volume = volume.astype("float32")
    return volume


def resize_volume(img, target_dim=(512, 512, 64)):
    """Resize across z-axis"""
    # Set the desired depth
    desired_depth = target_dim[2]
    desired_width = target_dim[1]
    desired_height = target_dim[0]
    # Get current depth
    current_depth = img.shape[-1]
    current_width = img.shape[0]
    current_height = img.shape[1]
    # Compute depth factor
    depth = current_depth / desired_depth
    width = current_width / desired_width
    height = current_height / desired_height
    depth_factor = 1 / depth
    width_factor = 1 / width
    height_factor = 1 / height
    img = ndimage.zoom(img, (width_factor, height_factor, depth_factor), order=1)
    return img


def remove_exterior(image):
    """Separate lung from background and paint background with mean color"""
    filt_image = cv2.GaussianBlur(image, (5, 5), 0)
    thresh = cv2.threshold(
        filt_image[filt_image > 0], 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU
    )[0]
    bin_image = np.uint8(filt_image > thresh)
    # Find body contour
    img, contours, hierarchy = cv2.findContours(
        bin_image.astype(np.uint8), cv2.RETR_CCOMP, cv2.CHAIN_APPROX_SIMPLE
    )
    areas = [cv2.contourArea(cnt) for cnt in contours]
    body_idx = np.argmax(areas)
    body_cont = contours[body_idx].squeeze()
    # Exclude external regions by replacing with bg mean
    body_mask = np.zeros(image.shape, dtype=np.uint8)
    cv2.drawContours(body_mask, [body_cont], 0, 1, -1)
    body_mask = body_mask.astype(bool)
    bg_mask = (~body_mask) & (image > 0)
    # exclude bright regions from mean
    bg_dark = bg_mask & ~bin_image
    bg_mean = np.mean(image[bg_dark])
    image[bg_mask] = bg_mean

    return image


def process_scan(path, target_dim=(512, 512, 64)):
    """Read and resize volume"""
    # Read scan
    volume = read_scan_path(path)
    # Normalize
    volume = volume * (1.0 / 255)
    volume = volume.astype("float32")
    # volume = normalize(volume)
    volume = resize_volume(volume, target_dim)
    return volume


def scan_stack(f, volume, rows=8, cols=8, start_with=0, show_every=1):
    import matplotlib.pyplot as plt
    from itertools import product

    fig, ax = plt.subplots(rows, cols, figsize=[24, 24])

    n = len(volume)
    prod = list(product(range(rows), range(cols)))
    prod = prod[:n]

    for i, (r, c) in enumerate(prod):
        ind = start_with + i * show_every
        ax[r, c].set_title("slice %d" % ind)
        ax[r, c].imshow(volume[:, :, ind], cmap="gray")
        ax[r, c].axis("off")
    # plt.subplots_adjust(wspace=0, hspace=0, left=0, right=1, bottom=0, top=1)
    plt.savefig(f, dpi=30)


#     plt.show()


df = pd.read_feather("ordered_scans.feather")
exams = df[df["lung_ordered"] == True]
exams = exams[exams["label"] != "CP"]
exams_train = exams[exams["new_split_class"] == "train"]
list_exams = exams_train["dataset"].values.tolist()
print("total ", len(list_exams))
for i, e in enumerate(list_exams[596:]):
    cls, pid, eid = e.split("/")[-3:]
    fname = "./exames_z64/{}_{}_{}.png".format(cls, pid, eid)
    scan = process_scan(e)
    scan_stack(fname, scan)
    print(i, fname)
