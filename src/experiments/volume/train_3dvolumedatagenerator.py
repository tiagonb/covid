#!/usr/bin/env python
# coding: utf-8

# This notebook run a 3D convnet for CT images agregated as volumetric data.

# In[1]:


import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
from tensorflow.keras.preprocessing.image import load_img, img_to_array
from tensorflow.keras.models import Sequential
import mlflow.keras

import os
import math
import zipfile
import numpy as np
import pandas as pd
from scipy import ndimage
from pathlib import Path
import cv2
import random

from skimage.transform import warp, AffineTransform

import argparse


# # data generator for volume

# In[2]:


class VolumeDataGenerator(keras.utils.Sequence):

    def __init__(self, list_IDs, labels, batch_size=10, augment=False, dim=(512, 512, 64), n_channels=1, n_classes=3, shuffle=True):
        self.dim = dim
        self.batch_size = batch_size
        self.labels = labels
        self.augment = augment
        self.list_IDs = list_IDs
        self.n_channels = n_channels
        self.n_classes = n_classes
        self.shuffle = shuffle
        self.on_epoch_end()

    def on_epoch_end(self):
        'Updates indexes after each epoch'
        self.indexes = np.arange(len(self.list_IDs))
        if self.shuffle == True:
            np.random.shuffle(self.indexes)

    def __len__(self):
        'Denotes the number of batches per epoch'
        return int(np.floor(len(self.list_IDs) / self.batch_size))

    def __data_generation(self, list_IDs_temp, labels_temp):
        # X : (n_samples, *dim, n_channels)
        'Generates data containing batch_size samples'
        # Initialization
        X = np.empty((self.batch_size, *self.dim, self.n_channels))
        y = np.empty((self.batch_size), dtype=int)

        # Generate data
        for i, (ID, label) in enumerate(zip(list_IDs_temp, labels_temp)):
            # Store sample
            scan = process_scan(ID, self.dim)
            if self.augment:
                scan = affine_augmentation(scan)
            # normalizing between 0 and 1
            vmin = scan.min()
            vmax = scan.max()
            scan = (scan - vmin) / (vmax - vmin)
            X[i, ..., 0] = scan
            # Store class
            y[i] = label

        return X, keras.utils.to_categorical(y, num_classes=self.n_classes)

    def __getitem__(self, index):
        'Generate one batch of data'
        # Generate indexes of the batch
        indexes = self.indexes[index*self.batch_size:(index+1)*self.batch_size]

        # Find list of IDs and labels
        list_IDs_temp = [self.list_IDs[k] for k in indexes]
        labels_temp = [self.labels[k] for k in indexes]

        # Generate data
        X, y = self.__data_generation(list_IDs_temp, labels_temp)

        return X, y


# # helper functions

# In[3]:


# example from: https://keras.io/examples/vision/3D_image_classification/
def read_scan_path(filepath):
    """Read files and load volume"""
    # Read files
    files = sorted(list(Path(filepath).glob('**/*')))
    imgs = []
#     print("### {} \nTotal of {} images.\n".format(filepath, len(files)))
    for i in files:
        img = cv2.imread(str(i), cv2.IMREAD_GRAYSCALE)
        #img = load_img(i, color_mode='grayscale')
        #x = img_to_array(np.squeeze(img))
        #img = remove_exterior(img)
        if hasattr(img, 'close'):
            img.close()
        imgs.append(img)

    scan = np.squeeze(np.stack(imgs, axis=2))
    return scan


def affine_augmentation(volume):
    '''
    Apply a sequence of affine transformations for data augmentation
    
    # Translation and then rotation
    coords_composite1 = R1 @ T1 @ coords
    # Rotation and then translation
    coords_composite2 = T1 @ R1 @ coords
    
    How the function is applied can be understood from right to left.
    '''
    w, h, d = volume.shape
    # scaling (scale images to 80-120% of their size, individually per axis)
    sx, sy = random.uniform(0.9, 1.1), random.uniform(0.9, 1.1)
    # image pixels center
    cx, cy = w / 2.0, h / 2.0
    mat_scale_centered = np.array(
        [[sx, 0, (1 - sx)*cx], [0, sy, (1 - sy)*cy], [0, 0, 1]])
    # rotation (range by -10 to +10 degrees)
    theta = random.uniform(-(np.pi/18), np.pi/18)
    mat_rotate = np.array([[1, 0, w/2], [0, 1, h/2], [0, 0, 1]]) @ np.array([[np.cos(theta), np.sin(theta), 0],
                                                                             [np.sin(theta), -np.cos(theta), 0], [0, 0, 1]]) @ np.array([[1, 0, -w/2], [0, 1, -h/2], [0, 0, 1]])
    # shear (by -10 to +10 degrees)
    shear = random.uniform(0, 0.1)
    mat_shear = np.array([[1, shear, 0], [shear, 1, 0], [0, 0, 1]])
    # translation by -10 to +10 percent (per axis)
    dx, dy = random.uniform(-(w * 0.05), w *
                            0.05), random.uniform(-(h * 0.05), h * 0.05)
    mat_shift = np.array([[1, 0, dx], [0, 1, dy], [0, 0, 1]])
    # applying transformations
    mat_all = mat_scale_centered @ mat_shear @ mat_shift @ mat_rotate
    for k in range(volume.shape[-1]):
        volume[...,k] = ndimage.affine_transform(volume[...,k], mat_all, mode='wrap')
        
    return volume


def resize_volume(img, target_dim=(512, 512, 64)):
    """Resize across z-axis"""
    # Set the desired depth
    desired_depth = target_dim[2]
    desired_width = target_dim[1]
    desired_height = target_dim[0]
    # Get current depth
    current_depth = img.shape[-1]
    current_width = img.shape[0]
    current_height = img.shape[1]
    # Compute depth factor
    depth = current_depth / desired_depth
    width = current_width / desired_width
    height = current_height / desired_height
    depth_factor = 1 / depth
    width_factor = 1 / width
    height_factor = 1 / height
    img = ndimage.zoom(
        img, (width_factor, height_factor, depth_factor), order=1)
    return img


def remove_exterior(image):
    """Separate lung from background and paint background with mean color"""
    filt_image = cv2.GaussianBlur(image, (5, 5), 0)
    thresh = cv2.threshold(
        filt_image[filt_image > 0], 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)[0]
    bin_image = np.uint8(filt_image > thresh)
    # Find body contour
    img, contours, hierarchy = cv2.findContours(bin_image.astype(
        np.uint8), cv2.RETR_CCOMP, cv2.CHAIN_APPROX_SIMPLE)
    areas = [cv2.contourArea(cnt) for cnt in contours]
    body_idx = np.argmax(areas)
    body_cont = contours[body_idx].squeeze()
    # Exclude external regions by replacing with bg mean
    body_mask = np.zeros(image.shape, dtype=np.uint8)
    cv2.drawContours(body_mask, [body_cont], 0, 1, -1)
    body_mask = body_mask.astype(bool)
    bg_mask = (~body_mask) & (image > 0)
    # exclude bright regions from mean
    bg_dark = bg_mask & ~bin_image
    bg_mean = np.mean(image[bg_dark])
    image[bg_mask] = bg_mean

    return image


def process_scan(path, target_dim=(512, 512, 64)):
    """Read and resize volume"""
    # Read scan
    volume = read_scan_path(path)
    # Normalize
    volume = volume.astype("float32")
    #volume = normalize(volume)
    volume = resize_volume(volume, target_dim)
    return volume



# # model definition

# In[5]:


def get_model(width=512, height=512, depth=64):
    """Build a 3D convolutional neural network model."""

    inputs = keras.Input((width, height, depth, 1))

    x = layers.Conv3D(filters=64, kernel_size=3, activation="relu")(inputs)
    x = layers.MaxPool3D(pool_size=2)(x)
    x = layers.BatchNormalization()(x)

    x = layers.Conv3D(filters=64, kernel_size=3, activation="relu")(inputs)
    x = layers.MaxPool3D(pool_size=2)(x)
    x = layers.BatchNormalization()(x)

    x = layers.Conv3D(filters=128, kernel_size=3, activation="relu")(x)
    x = layers.MaxPool3D(pool_size=2)(x)
    x = layers.BatchNormalization()(x)

    x = layers.Conv3D(filters=256, kernel_size=3, activation="relu")(x)
    x = layers.MaxPool3D(pool_size=2)(x)
    x = layers.BatchNormalization()(x)

    x = layers.GlobalAveragePooling3D()(x)
    x = layers.Dense(units=512, activation="relu")(x)
    x = layers.Dropout(0.3)(x)

    outputs = layers.Dense(units=3, activation="softmax")(x)

    # Define the model.
    model = keras.Model(inputs, outputs, name="3dcnn")
    return model


# In[6]:


gpus = tf.config.experimental.list_physical_devices('GPU')
if gpus:
    try:
        # Currently, memory growth needs to be the same across GPUs
        for gpu in gpus:
            tf.config.experimental.set_memory_growth(gpu, True)
        logical_gpus = tf.config.experimental.list_logical_devices('GPU')
        print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPUs")
    except RuntimeError as e:
        # Memory growth must be set before GPUs have been initialized
        print(e)
tf.test.gpu_device_name()

# fname = os.path.sep.join([args["weights"], "weights-{epoch:03d}-{val_loss:.4f}.hdf5"])
parser = argparse.ArgumentParser(description='Process covidnet train input parameters.')
parser.add_argument("-x", "--experiment-name", type=str, default="exp4", help="choose name for experiment")
parser.add_argument("-n", "--run-name", type=str, default="", help="choose name for this run")
parser.add_argument("-t", "--tags", type=str, default="", help="set tags in training runs")
args = vars(parser.parse_args())

run_name = args['run_name']

# Parameters
params = {'dim': (260, 260, 64),
          'batch_size': 1,
          'n_classes': 3,
          'n_channels': 1,
          'shuffle': True}

# Datasets
# df = pd.read_feather('predictions_all_info.feather')
# df_cut = df[df['model'] == '/misc/users/tiagonb/covid-net/models/COVID-Net_CT-1_S']
# exams = df_cut[['new_split_class','dataset', 'label']]
# exams = exams.drop_duplicates()

df = pd.read_feather('ordered_scans.feather')
exams = df[df['lung_ordered'] == True]
# Convert to categorical var
exams['label'] = exams['label'].astype('category')
exams['label_cat'] = exams['label'].cat.codes
# Split train, val and test
exams_train = exams[exams['new_split_class'] == 'train']
exams_val = exams[exams['new_split_class'] == 'val']
exams_test = exams[exams['new_split_class'] == 'test']

# Generators
training_generator = VolumeDataGenerator(exams_train['dataset'].values.tolist(),
                                         exams_train['label_cat'].values.tolist(), augment=True, **params)
validation_generator = VolumeDataGenerator(exams_val['dataset'].values.tolist(),
                                           exams_val['label_cat'].values.tolist(), **params)
testing_generator = VolumeDataGenerator(exams_test['dataset'].values.tolist(),
                                           exams_test['label_cat'].values.tolist(), **params)

n_samples_train = len(exams_train.index)
n_samples_validation = len(exams_val.index)
steps_per_epoch_train = int(
    math.ceil(1. * n_samples_train / params['batch_size']))
steps_per_epoch_validation = int(
    math.ceil(1. * n_samples_validation / params['batch_size']))

# Build model.
model = get_model(width=260, height=260, depth=64)
print("###############################################################################33")
model.summary()

# Compile model.
initial_learning_rate = 0.0001
lr_schedule = keras.optimizers.schedules.ExponentialDecay(
    initial_learning_rate, decay_steps=100000, decay_rate=0.96, staircase=True
)

model.compile(
    loss="categorical_crossentropy",
    optimizer=keras.optimizers.Adam(learning_rate=lr_schedule),
    metrics=["acc"],
)

history = model.fit_generator(generator=training_generator,
          steps_per_epoch=steps_per_epoch_train,
          validation_data=validation_generator,
          validation_steps=steps_per_epoch_validation,
          epochs=10, verbose=2)

acc = history.history['acc']
val_acc = history.history['val_acc']
loss = history.history['loss']
val_loss = history.history['val_loss']

