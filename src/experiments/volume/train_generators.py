# -*- coding: utf-8 -*-

"""
Script used to train CNN on slices (images) using generators as input to the trainig loop
"""

# from tensorflow import keras
# import keras
# from tensorflow.keras import models
# from tensorflow.keras import layers
# from tensorflow.keras import optimizers
# from tensorflow.keras.preprocessing.image import ImageDataGenerator

# from keras import layers
# from keras import models
from keras import optimizers
from keras.preprocessing.image import ImageDataGenerator
from sklearn.utils import class_weight
import mlflow.keras

from models.models import (
    CatsnDogs,
    CatsnDogsDropout,
    ResNet50Custom,
    PaperNet,
    EarlyBifurcation,
    LaterBifurcation,
)
from utils.figures import plot_trainval_metrics, show_predictions

import numpy as np
import os
import pathlib
import math
import argparse

import tensorflow as tf

gpus = tf.config.experimental.list_physical_devices("GPU")
if gpus:
    try:
        # Currently, memory growth needs to be the same across GPUs
        for gpu in gpus:
            tf.config.experimental.set_memory_growth(gpu, True)
        logical_gpus = tf.config.experimental.list_logical_devices("GPU")
        print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPUs")
    except RuntimeError as e:
        # Memory growth must be set before GPUs have been initialized
        print(e)
tf.test.gpu_device_name()

# fname = os.path.sep.join([args["weights"], "weights-{epoch:03d}-{val_loss:.4f}.hdf5"])
parser = argparse.ArgumentParser(description="Process covidnet train input parameters.")
parser.add_argument(
    "-d",
    "--dataset",
    type=str,
    default="../toy_data3_double",
    help="path to input dataset",
)
parser.add_argument(
    "-r",
    "--model-name",
    type=str,
    default="CatsnDogs",
    help="model from .conv to run experiments",
)
parser.add_argument(
    "-o",
    "--optimizer",
    type=str,
    default="Adam",
    help="choose optimizer (Adam, RMS, SGD)",
)
parser.add_argument(
    "-x",
    "--experiment-name",
    type=str,
    default="exp4",
    help="choose name for experiment",
)
parser.add_argument(
    "-n", "--run-name", type=str, default="", help="choose name for this run"
)
parser.add_argument("-l", "--lr", type=float, default=0.001, help="learning rate")
parser.add_argument("-e", "--epochs", type=int, default=100, help="number of epochs")
parser.add_argument("-b", "--batch-size", type=int, default=20, help="size of batch")
parser.add_argument(
    "-t", "--tags", type=str, default="", help="set tags in training runs"
)
args = vars(parser.parse_args())


long_name = "{}[{}](opt={},lr={},e={},b={},d={})".format(
    args["experiment_name"],
    args["model_name"],
    args["optimizer"],
    args["lr"],
    args["epochs"],
    args["batch_size"],
    args["dataset"].split("/")[-1],
)

run_name = args["run_name"]

data_path_str = args["dataset"]
txt_path_str = args["dataset"]


"""Data Paths"""

data_path = pathlib.Path(data_path_str)

train_path = data_path / "train"
val_path = data_path / "val"
test_path = data_path / "test"


"""Neural Network"""

########################
# Create a MirroredStrategy for multiple GPU
# strategy = tf.distribute.MirroredStrategy()
# print("Number of devices: {}".format(strategy.num_replicas_in_sync))

# Open a strategy scope. Build and compile model inside this scope
# with strategy.scope():
########################

if args["model_name"] == "CatsnDogsDropout":
    model = CatsnDogsDropout.build(width=512, height=512, depth=1, classes=2)
elif args["model_name"] == "CatsnDogs":
    model = CatsnDogs.build(width=512, height=512, depth=1, classes=2)
elif args["model_name"] == "ResNet50Custom":
    model = ResNet50Custom.build(width=512, height=512, depth=1, classes=3)
elif args["model_name"] == "PaperNet":
    model = PaperNet.build(width=512, height=512, depth=1, classes=3)
elif args["model_name"] == "EarlyBifurcation":
    model = EarlyBifurcation.build(width=512, height=512, depth=1, classes=3)
elif args["model_name"] == "LaterBifurcation":
    model = LaterBifurcation.build(width=512, height=512, depth=1, classes=3)
else:
    raise AssertionError(
        "The --model-name command line argument should be a valid model"
    )

model.summary()

if args["optimizer"] == "SGD":
    optimizer = optimizers.SGD(lr=args["lr"])
elif args["optimizer"] == "RMS":
    optimizer = optimizers.RMSprop(lr=args["lr"])
elif args["optimizer"] == "Adam":
    optimizer = optimizers.Adam(lr=args["lr"])
else:
    raise AssertionError(
        "The --optimizer command line argument should be a valid optimizer"
    )

model.compile(loss="binary_crossentropy", optimizer=optimizer, metrics=["acc"])


# os.chdir("/content/gdrive/My Drive/covidnet/")
# model.save_weights('model_weights_dogs_cats.h5')


"""Data Preprocessing (train and validation)"""

BATCH_SIZE = args["batch_size"]
EPOCHS = args["epochs"]

train_datagen = ImageDataGenerator(
    rescale=1.0 / 255,
    rotation_range=10,
    width_shift_range=[-0.1, 0.1],
    height_shift_range=[-0.1, 0.1],
    shear_range=0.15,
    zoom_range=[0.8, 1.2],
)

val_datagen = ImageDataGenerator(rescale=1.0 / 255)

train_generator = train_datagen.flow_from_directory(
    train_path,
    color_mode="grayscale",
    target_size=(512, 512),
    batch_size=BATCH_SIZE,
    class_mode="binary",
)

y_train_cls = train_generator.classes
class_weights = class_weight.compute_class_weight(
    "balanced", np.unique(y_train_cls), y_train_cls
)
class_weights = dict(zip(range(len(class_weights)), class_weights))

validation_generator = val_datagen.flow_from_directory(
    val_path,
    color_mode="grayscale",
    target_size=(512, 512),
    batch_size=BATCH_SIZE,
    class_mode="binary",
)

n_samples_train = train_generator.samples
n_samples_validation = validation_generator.samples

steps_per_epoch_train = int(math.ceil(1.0 * n_samples_train / BATCH_SIZE))
steps_per_epoch_validation = int(math.ceil(1.0 * n_samples_validation / BATCH_SIZE))

print("run name: ", run_name)
print("class weights: ", class_weights)
print(
    "training_size:{}, batch_size:{}, epochs:{}, steps_per_epoch:{}".format(
        n_samples_train, BATCH_SIZE, EPOCHS, steps_per_epoch_train
    )
)
print(
    "validation_size:{}, batch_size:{}, epochs:{}, steps_per_epoch:{}".format(
        n_samples_validation, BATCH_SIZE, EPOCHS, steps_per_epoch_validation
    )
)


try:
    experiment_id = mlflow.create_experiment(name=args["experiment_name"])
except:
    experiment_id = mlflow.get_experiment_by_name(
        name=args["experiment_name"]
    ).experiment_id


"""Running model (train with validation)"""

with mlflow.start_run(experiment_id=experiment_id, run_name=run_name) as run:
    # mlflow.tensorflow.autolog(every_n_iter=1)
    mlflow.keras.autolog()

    history = model.fit(
        train_generator,
        steps_per_epoch=steps_per_epoch_train,
        epochs=EPOCHS,
        validation_data=validation_generator,
        validation_steps=steps_per_epoch_validation,
        class_weight=class_weights,
    )

    """Plotting results"""

    acc = history.history["acc"]
    val_acc = history.history["val_acc"]
    loss = history.history["loss"]
    val_loss = history.history["val_loss"]

    figname = plot_trainval_metrics(acc, val_acc, "accuracy", long_name)
    mlflow.log_artifact(figname)
    figname = plot_trainval_metrics(loss, val_loss, "loss", long_name)
    mlflow.log_artifact(figname)

    # set tags
    if args["tags"]:
        for tag in args["tags"].split(","):
            tag_k, tag_v = tag.split("=")
            mlflow.set_tag(tag_k, tag_v)
