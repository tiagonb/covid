# -*- coding: utf-8 -*-

"""
Script used to fine tunne Resnet50 on slices (images) using generators as input to the trainig loop
"""

# from tensorflow import keras
# import keras
# from tensorflow.keras import models
# from tensorflow.keras import layers
# from tensorflow.keras import optimizers
# from tensorflow.keras.preprocessing.image import ImageDataGenerator

from keras import layers
from keras import models
from keras import optimizers
from keras.preprocessing.image import ImageDataGenerator
from sklearn.utils import class_weight
import mlflow.keras

# from models.models import CatsnDogs, CatsnDogsDropout, ResNet50Custom, PaperNet
from utils.figures import plot_trainval_metrics, show_predictions
from keras.applications.resnet50 import (
    ResNet50,
    preprocess_input as preprocess_input_resnet50,
)
from keras.applications.vgg16 import VGG16, preprocess_input as preprocess_input_vgg16

# from keras.applications import VGG16
# from keras.applications import ResNet50

import numpy as np
import os
import pathlib
import math
import argparse

import tensorflow as tf

gpus = tf.config.experimental.list_physical_devices("GPU")
if gpus:
    try:
        # Currently, memory growth needs to be the same across GPUs
        for gpu in gpus:
            tf.config.experimental.set_memory_growth(gpu, True)
        logical_gpus = tf.config.experimental.list_logical_devices("GPU")
        print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPUs")
    except RuntimeError as e:
        # Memory growth must be set before GPUs have been initialized
        print(e)
tf.test.gpu_device_name()

# fname = os.path.sep.join([args["weights"], "weights-{epoch:03d}-{val_loss:.4f}.hdf5"])
parser = argparse.ArgumentParser(description="Process covidnet train input parameters.")
parser.add_argument(
    "-d",
    "--dataset",
    type=str,
    default="../toy_data3_double",
    help="path to input dataset",
)
parser.add_argument(
    "-r",
    "--model-name",
    type=str,
    default="CatsnDogs",
    help="model from .conv to run experiments",
)
parser.add_argument(
    "-o",
    "--optimizer",
    type=str,
    default="Adam",
    help="choose optimizer (Adam, RMS, SGD)",
)
parser.add_argument(
    "-x",
    "--experiment-name",
    type=str,
    default="exp",
    help="choose name for experiment",
)
parser.add_argument(
    "-n", "--run-name", type=str, default="", help="choose name for this run"
)
parser.add_argument("-l", "--lr", type=float, default=0.0001, help="learning rate")
parser.add_argument("-e", "--epochs", type=int, default=30, help="number of epochs")
parser.add_argument("-b", "--batch-size", type=int, default=20, help="size of batch")
parser.add_argument(
    "-t", "--tags", type=str, default="", help="set tags in training runs"
)
args = vars(parser.parse_args())


long_name = "{}[{}](opt={},lr={},e={},b={},d={})".format(
    args["experiment_name"],
    args["model_name"],
    args["optimizer"],
    args["lr"],
    args["epochs"],
    args["batch_size"],
    args["dataset"].split("/")[-1],
)

run_name = args["run_name"]

data_path_str = args["dataset"]
txt_path_str = args["dataset"]


"""Data Paths"""

data_path = pathlib.Path(data_path_str)

train_path = data_path / "train"
val_path = data_path / "val"
test_path = data_path / "test"


"""Neural Network"""

########################
# Create a MirroredStrategy for multiple GPU
# strategy = tf.distribute.MirroredStrategy()
# print("Number of devices: {}".format(strategy.num_replicas_in_sync))

# Open a strategy scope. Build and compile model inside this scope
# with strategy.scope():
########################

if args["model_name"] == "VGG16":
    # baseModel = VGG16(weights='imagenet', include_top=False, input_tensor=layers.Input(shape=(512, 512, 3)))
    baseModel = VGG16(weights="imagenet", include_top=False, input_shape=(512, 512, 3))

    headModel = baseModel.output
    headModel = layers.AveragePooling2D(pool_size=(7, 7))(headModel)
    headModel = layers.Flatten(name="flatten")(headModel)
    headModel = layers.Dense(256, activation="relu")(headModel)
    headModel = layers.Dropout(0.5)(headModel)
    # headModel = layers.Dense(3, activation="softmax")(headModel)
    headModel = layers.Dense(1, activation="sigmoid")(headModel)
    model = models.Model(inputs=baseModel.input, outputs=headModel)

    baseModel.trainable = False

    preprocess_input = preprocess_input_vgg16

elif args["model_name"] == "ResNet50":
    baseModel = ResNet50(
        weights="imagenet",
        include_top=False,
        input_tensor=layers.Input(shape=(512, 512, 3)),
    )

    headModel = baseModel.output
    headModel = layers.AveragePooling2D(pool_size=(7, 7))(headModel)
    headModel = layers.Flatten(name="flatten")(headModel)
    headModel = layers.Dense(256, activation="relu")(headModel)
    headModel = layers.Dropout(0.5)(headModel)
    # headModel = layers.Dense(3, activation="softmax")(headModel)
    headModel = layers.Dense(1, activation="sigmoid")(headModel)
    model = models.Model(inputs=baseModel.input, outputs=headModel)

    baseModel.trainable = False

    preprocess_input = preprocess_input_resnet50
else:
    raise AssertionError(
        "The --model-name command line argument should be a valid model"
    )

model.summary()

if args["optimizer"] == "SGD":
    optimizer = optimizers.SGD(lr=args["lr"])
elif args["optimizer"] == "RMS":
    optimizer = optimizers.RMSprop(lr=args["lr"])
elif args["optimizer"] == "Adam":
    optimizer = optimizers.Adam(lr=args["lr"])
else:
    raise AssertionError(
        "The --optimizer command line argument should be a valid optimizer"
    )

model.compile(loss="binary_crossentropy", optimizer=optimizer, metrics=["acc"])


"""Data Preprocessing (train and validation)"""

BATCH_SIZE = args["batch_size"]
EPOCHS = args["epochs"]

train_datagen = ImageDataGenerator(
    preprocessing_function=preprocess_input,
    rescale=1.0 / 255,
    rotation_range=10,
    width_shift_range=[-0.1, 0.1],
    height_shift_range=[-0.1, 0.1],
    shear_range=0.15,
    zoom_range=[0.8, 1.2],
)

val_datagen = ImageDataGenerator(
    preprocessing_function=preprocess_input, rescale=1.0 / 255
)

train_generator = train_datagen.flow_from_directory(
    train_path, target_size=(512, 512), batch_size=BATCH_SIZE, class_mode="binary"
)

y_train_cls = train_generator.classes
class_weights = class_weight.compute_class_weight(
    "balanced", np.unique(y_train_cls), y_train_cls
)
class_weights = dict(zip(range(len(class_weights)), class_weights))

validation_generator = val_datagen.flow_from_directory(
    val_path, target_size=(512, 512), batch_size=BATCH_SIZE, class_mode="binary"
)

n_samples_train = train_generator.samples
n_samples_validation = validation_generator.samples

steps_per_epoch_train = int(math.ceil(1.0 * n_samples_train / BATCH_SIZE))
steps_per_epoch_validation = int(math.ceil(1.0 * n_samples_validation / BATCH_SIZE))

print("run name: ", run_name)
print("class weights: ", class_weights)
print(
    "training_size:{}, batch_size:{}, epochs:{}, steps_per_epoch:{}".format(
        n_samples_train, BATCH_SIZE, EPOCHS, steps_per_epoch_train
    )
)
print(
    "validation_size:{}, batch_size:{}, epochs:{}, steps_per_epoch:{}".format(
        n_samples_validation, BATCH_SIZE, EPOCHS, steps_per_epoch_validation
    )
)


try:
    experiment_id = mlflow.create_experiment(name=args["experiment_name"])
except:
    experiment_id = mlflow.get_experiment_by_name(
        name=args["experiment_name"]
    ).experiment_id


"""Running model (train with validation)"""

with mlflow.start_run(experiment_id=experiment_id, run_name=run_name) as run:
    # mlflow.tensorflow.autolog(every_n_iter=1)
    mlflow.keras.autolog()

    history1 = model.fit(
        train_generator,
        steps_per_epoch=steps_per_epoch_train,
        epochs=EPOCHS,
        validation_data=validation_generator,
        validation_steps=steps_per_epoch_validation,
        class_weight=class_weights,
    )

    acc1 = history1.history["acc"]
    val_acc1 = history1.history["val_acc"]
    loss1 = history1.history["loss"]
    val_loss1 = history1.history["val_loss"]

    """ Fine tunning """

    baseModel.trainable = True
    history2 = model.fit(
        train_generator,
        steps_per_epoch=steps_per_epoch_train,
        epochs=10,
        validation_data=validation_generator,
        validation_steps=steps_per_epoch_validation,
        class_weight=class_weights,
    )

    acc2 = history2.history["acc"]
    val_acc2 = history2.history["val_acc"]
    loss2 = history2.history["loss"]
    val_loss2 = history2.history["val_loss"]

    acc_total = acc1 + acc2
    val_acc_total = val_acc1 + val_acc2
    loss_total = loss1 + loss2
    val_loss_total = val_loss1 + val_loss2

    for idx in range(len(acc_total)):
        mlflow.log_metrics(
            {
                "acc_total": acc_total[idx],
                "val_acc_total": val_acc_total[idx],
                "loss_total": loss_total[idx],
                "val_loss_total": val_loss_total[idx],
            },
            step=idx + 1,
        )

    """Plotting results"""

    # figname = plot_trainval_metrics(acc, val_acc, 'accuracy', run_name)
    # mlflow.log_artifact(figname)
    # figname = plot_trainval_metrics(loss, val_loss, 'loss', run_name)
    # mlflow.log_artifact(figname)

    # set tags
    if args["tags"]:
        for tag in args["tags"].split(","):
            tag_k, tag_v = tag.split("=")
            mlflow.set_tag(tag_k, tag_v)
