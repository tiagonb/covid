import pathlib
import shutil

from_dir = '/misc/users/tiagonb/covid-net/source/COVIDNet-CT/data'
from_dir = pathlib.Path(from_dir)

to_dir = '/misc/users/tiagonb/covid-net/tools/full_dataset'
txt_path = pathlib.Path(to_dir)
to_dir = pathlib.Path(to_dir)
(to_dir/"train"/"Normal").mkdir(parents=True, exist_ok=True)
(to_dir/"train"/"NCP").mkdir(parents=True, exist_ok=True)
(to_dir/"train"/"CP").mkdir(parents=True, exist_ok=True)
(to_dir/"val"/"Normal").mkdir(parents=True, exist_ok=True)
(to_dir/"val"/"NCP").mkdir(parents=True, exist_ok=True)
(to_dir/"val"/"CP").mkdir(parents=True, exist_ok=True)
(to_dir/"test"/"Normal").mkdir(parents=True, exist_ok=True)
(to_dir/"test"/"NCP").mkdir(parents=True, exist_ok=True)
(to_dir/"test"/"CP").mkdir(parents=True, exist_ok=True)

p_train = txt_path/'train.txt'
p_val = txt_path/'val.txt'
p_test = txt_path/'test.txt'

p_train = p_train.read_text()
for i in p_train.split('\n'):
    if i:
        f = i.split()[0]
        from_dir_file = pathlib.Path(from_dir/f)
        to_dir_file = pathlib.Path(to_dir/'train'/f)
        shutil.copy(from_dir_file, to_dir_file)

files = list((pathlib.Path(to_dir/'train')).rglob("Normal*.png"))
for i in files:
    shutil.move(i, i.parent / "Normal" / i.name)
files = list((pathlib.Path(to_dir/'train')).rglob("NCP*.png"))
for i in files:
    shutil.move(i, i.parent / "NCP" / i.name)
files = list((pathlib.Path(to_dir/'train')).rglob("CP*.png"))
for i in files:
    shutil.move(i, i.parent / "CP" / i.name)

p_val = p_val.read_text()
for i in p_val.split('\n'):
    if i:
        f = i.split()[0]
        from_dir_file = pathlib.Path(from_dir/f)
        to_dir_file = pathlib.Path(to_dir/'val'/f)
        shutil.copy(from_dir_file, to_dir_file)

files = list((pathlib.Path(to_dir/'val')).rglob("Normal*.png"))
for i in files:
    shutil.move(i, i.parent / "Normal" / i.name)
files = list((pathlib.Path(to_dir/'val')).rglob("NCP*.png"))
for i in files:
    shutil.move(i, i.parent / "NCP" / i.name)
files = list((pathlib.Path(to_dir/'val')).rglob("CP*.png"))
for i in files:
    shutil.move(i, i.parent / "CP" / i.name)
        
p_test = p_test.read_text()
for i in p_test.split('\n'):
    if i:
        f = i.split()[0]
        from_dir_file = pathlib.Path(from_dir/f)
        to_dir_file = pathlib.Path(to_dir/'test'/f)
        shutil.copy(from_dir_file, to_dir_file)

files = list((pathlib.Path(to_dir/'test')).rglob("Normal*.png"))
for i in files:
    shutil.move(i, i.parent / "Normal" / i.name)
files = list((pathlib.Path(to_dir/'test')).rglob("NCP*.png"))
for i in files:
    shutil.move(i, i.parent / "NCP" / i.name)
files = list((pathlib.Path(to_dir/'test')).rglob("CP*.png"))
for i in files:
    shutil.move(i, i.parent / "CP" / i.name)
