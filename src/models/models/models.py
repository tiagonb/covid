from keras import layers
from keras import models
from keras import Input
from keras.applications import ResNet50
#from tensorflow.keras import models
#from tensorflow.keras import layers
#from tensorflow.keras.applications import ResNet50


class CatsnDogs:
    @staticmethod
    def build(width, height, depth, classes):
        input_shape = (height, width, depth)

        model = models.Sequential()
        model.add(layers.Conv2D(32, (3, 3), activation='relu',
                                input_shape=input_shape))
        model.add(layers.MaxPooling2D((2, 2)))
        model.add(layers.Conv2D(64, (3, 3), activation='relu'))
        model.add(layers.MaxPooling2D((2, 2)))
        model.add(layers.Conv2D(128, (3, 3), activation='relu'))
        model.add(layers.MaxPooling2D((2, 2)))
        model.add(layers.Conv2D(128, (3, 3), activation='relu'))
        model.add(layers.MaxPooling2D((2, 2)))
        model.add(layers.Flatten())
        model.add(layers.Dense(512, activation='relu'))
        model.add(layers.Dense(classes, activation='softmax'))

        return model


class CatsnDogsDropout:
    @staticmethod
    def build(width, height, depth, classes):
        input_shape = (height, width, depth)

        model = models.Sequential()
        model.add(layers.Conv2D(32, (3, 3), activation='relu',
                                input_shape=input_shape))
        model.add(layers.MaxPooling2D((2, 2)))
        model.add(layers.Conv2D(64, (3, 3), activation='relu'))
        model.add(layers.MaxPooling2D((2, 2)))
        model.add(layers.Conv2D(128, (3, 3), activation='relu'))
        model.add(layers.MaxPooling2D((2, 2)))
        model.add(layers.Conv2D(128, (3, 3), activation='relu'))
        model.add(layers.MaxPooling2D((2, 2)))
        model.add(layers.Flatten())
        model.add(layers.Dropout(0.5))
        model.add(layers.Dense(512, activation='relu'))
        model.add(layers.Dense(classes, activation='softmax'))

        return model


class ResNet50Custom:
    @staticmethod
    def build(width, height, depth, classes):
        input_shape = (height, width, depth)

        baseModel = ResNet50(weights=None, include_top=False, input_tensor=layers.Input(shape=input_shape))
        headModel = baseModel.output
        headModel = layers.AveragePooling2D(pool_size=(7, 7))(headModel)
        headModel = layers.Flatten(name="flatten")(headModel)
        headModel = layers.Dense(256, activation="relu")(headModel)
        headModel = layers.Dropout(0.5)(headModel)
        headModel = layers.Dense(classes, activation="softmax")(headModel)
        model = models.Model(inputs=baseModel.input, outputs=headModel)

        return model


class PaperNet:
    @staticmethod
    def build(width, height, depth, classes):
        input_shape = (height, width, depth)

        model = models.Sequential()
        model.add(layers.Conv2D(64, (3, 3), activation='relu',
                                input_shape=input_shape))
        model.add(layers.MaxPooling2D((2, 2)))
        model.add(layers.BatchNormalization())

        model.add(layers.Conv2D(64, (3, 3), activation='relu'))
        model.add(layers.MaxPooling2D((2, 2)))
        model.add(layers.BatchNormalization())

        model.add(layers.Conv2D(128, (3, 3), activation='relu'))
        model.add(layers.MaxPooling2D((2, 2)))
        model.add(layers.BatchNormalization())
        
        model.add(layers.Conv2D(256, (3, 3), activation='relu'))
        model.add(layers.MaxPooling2D((2, 2)))
        model.add(layers.BatchNormalization())

        model.add(layers.GlobalAveragePooling2D())
        model.add(layers.Dense(512, activation='relu'))
        model.add(layers.Dropout(0.3))
        model.add(layers.Dense(classes, activation='softmax'))

        return model


class EarlyBifurcation:
    @staticmethod
    def build(width, height, depth, classes):
        input_tensor = Input(shape=(height, width, depth))
        x = layers.Convolution2D(32, (3, 3), activation='relu')(input_tensor)
        x = layers.MaxPooling2D(pool_size=(2, 2))(x)

        left = layers.Convolution2D(64, (1, 1), padding='valid', activation='relu')(x)
        right = layers.Convolution2D(64, (3, 3), padding='same', activation='relu')(x)
        x = layers.concatenate([left, right])
        x = layers.MaxPooling2D(pool_size=(3, 3), strides=(2, 2))(x)

        x = layers.Convolution2D(64, (3, 3), activation='relu')(x)
        x = layers.MaxPooling2D(pool_size=(2, 2))(x)

        x = layers.Convolution2D(128, (3, 3), activation='relu')(x)
        x = layers.MaxPooling2D(pool_size=(2, 2))(x)

        x = layers.Convolution2D(128, (3, 3), activation='relu')(x)
        x = layers.MaxPooling2D(pool_size=(2, 2))(x)

        x = layers.Flatten()(x)
        x = layers.Dropout(0.5)(x)
        x = layers.Dense(512, activation='relu')(x)
        output_tensor = layers.Dense(classes, activation='softmax')(x)

        model = keras.Model(inputs=input_tensor, outputs=output_tensor)

        return model

class LaterBifurcation:
    @staticmethod
    def build(width, height, depth, classes):
        input_tensor = Input(shape=(height, width, depth))
        x = layers.Convolution2D(32, (3, 3), activation='relu')(input_tensor)
        x = layers.MaxPooling2D(pool_size=(2, 2))(x)

        x = layers.Convolution2D(64, (3, 3), activation='relu')(x)
        x = layers.MaxPooling2D(pool_size=(2, 2))(x)

        x = layers.Convolution2D(128, (3, 3), activation='relu')(x)
        x = layers.MaxPooling2D(pool_size=(2, 2))(x)

        x = layers.Convolution2D(128, (3, 3), activation='relu')(x)
        x = layers.MaxPooling2D(pool_size=(2, 2))(x)

        left = layers.Convolution2D(64, (1, 1), padding='valid', activation='relu')(x)
        right = layers.Convolution2D(64, (3, 3), padding='same', activation='relu')(x)
        x = layers.concatenate([left, right])
        x = layers.MaxPooling2D(pool_size=(3, 3), strides=(2, 2))(x)

        x = layers.Flatten()(x)
        x = layers.Dropout(0.5)(x)
        x = layers.Dense(512, activation='relu')(x)
        output_tensor = layers.Dense(classes, activation='softmax')(x)

        model = keras.Model(inputs=input_tensor, outputs=output_tensor)

        return model

