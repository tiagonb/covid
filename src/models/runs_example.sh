###################### example calling experiments ###############################
python train.py --experiment-name "exp7" --run-name "cnd-d7c2-nbox" --no-bbox --epochs 60 --batch-size 20 --model-name CatsnDogs --dataset "../toy_data7_cluster2" --tags "bbox=no,aug=yes"

python train.py --experiment-name "exp7" --run-name "cndd-d7c2-nbox" --no-bbox --epochs 60 --batch-size 20 --model-name CatsnDogsDropout --dataset "../toy_data7_cluster2" --tags "bbox=no,aug=yes"

python train_with_transfer.py --experiment-name "exp7" --run-name "rn50-dd7c2-nbox" --no-bbox --epochs 60 --batch-size 5 --model-name ResNet50 --dataset "../toy_data7_cluster2" --tags "bbox=no,aug=yes"







