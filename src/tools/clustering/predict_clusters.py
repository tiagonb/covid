#!/usr/bin/env python
# coding: utf-8

# for loading/processing the images  
from tensorflow.keras.preprocessing.image import load_img, img_to_array
from tensorflow.keras.applications.resnet50 import preprocess_input 
from tensorflow.keras.preprocessing.image import ImageDataGenerator

# models 
from tensorflow.keras.applications.resnet50 import ResNet50 
from tensorflow.keras.models import Model, load_model

from tensorflow.keras import layers
from tensorflow.keras import models
from tensorflow.keras.applications import VGG16

# for everything else
import os
import numpy as np
import matplotlib.pyplot as plt
from random import randint
import pandas as pd
import pickle
import pathlib
import argparse

import tensorflow as tf
gpus = tf.config.experimental.list_physical_devices('GPU')
if gpus:
    try:
        # Currently, memory growth needs to be the same across GPUs
        for gpu in gpus:
          tf.config.experimental.set_memory_growth(gpu, True)
        logical_gpus = tf.config.experimental.list_logical_devices('GPU')
        print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPUs")
    except RuntimeError as e:
        # Memory growth must be set before GPUs have been initialized
        print(e)
tf.test.gpu_device_name()


"""Parsing input"""

parser = argparse.ArgumentParser(description='Process covidnet evaluate input parameters.')
parser.add_argument("-d", "--dataset", type=str, default="", help="path to images")
args = vars(parser.parse_args())

data_path = args['dataset']


"""Loading model"""

baseModel = VGG16(weights='imagenet', include_top=False, input_tensor=layers.Input(shape=(512, 512, 3)))

headModel = baseModel.output
headModel = layers.AveragePooling2D(pool_size=(7, 7))(headModel)
headModel = layers.Flatten(name="flatten")(headModel)
headModel = layers.Dense(256, activation="relu")(headModel)
headModel = layers.Dropout(0.5)(headModel)
headModel = layers.Dense(4, activation="softmax")(headModel)

model = models.Model(inputs=baseModel.input, outputs=headModel)
model.load_weights('VGG16_fine_tunned_for_clustering_2.h5')
model.summary()


"""Data Paths"""

data_path = pathlib.Path(data_path)
# keras trick - remove last dir and used it in flow_from_directory classes
last_dir = data_path.absolute().name
data_path = pathlib.Path(data_path.absolute().parent)


"""Model Predictions (data_path)"""

pred_datagen = ImageDataGenerator(rescale=1./255)

pred_generator = pred_datagen.flow_from_directory(
        data_path,
        color_mode="rgb",
        target_size=(512, 512),
        batch_size=1,
        shuffle = False,
        classes=[last_dir],
        class_mode='categorical')

filepaths = pred_generator.filepaths
n_samples = len(filepaths)

pred = model.predict(pred_generator, steps = n_samples)


"""Post process predictions"""

labels = {0: 'superior_lung', 1: 'bad_image', 2: 'middle_lung', 3: 'inferior_lung'}
predicted_class_indices = np.argmax(pred,axis=1)
predicted_class = [labels[k] for k in predicted_class_indices]

pred_df = pd.DataFrame({'files': filepaths, 
                     'prob_suplung': pred[:,0],
                     'prob_badimg': pred[:,1],
                     'prob_midlung': pred[:,2],
                     'prob_influng': pred[:,3],
                     'pred_lungclass': predicted_class})


print("feather file modifications (rows, columns):")
print("pred_df shape", pred_df.shape)

p = pathlib.Path("./predictions_clusters.feather")
if p.is_file():
    data = pd.read_feather(p)
    print("loaded data shape", data.shape)
    data = pd.concat([data, pred_df], ignore_index=True)
    print("removed data shape", data.shape)
    data.drop_duplicates(subset=['files'], keep='last', inplace=True)
    print("concated data shape", data.shape)
    data.to_feather(p)
else:
    pred_df.to_feather(p)