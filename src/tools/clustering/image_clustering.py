#!/usr/bin/env python
# coding: utf-8

####################################################################################################
# from: https://towardsdatascience.com/how-to-cluster-images-based-on-visual-similarity-cd6e7209fe34
#
# VGG16 feature shape — (1L, 7L, 7L, 512L)
# VGG19 feature shape — (1L, 7L, 7L, 512L)
# InceptionV3 feature shape — (1L, 5L, 5L, 2048L)
# ResNet50 feature shape — (1L, 1L, 1L, 2048L)
####################################################################################################

# for loading/processing the images
from tensorflow.keras.preprocessing.image import load_img
from tensorflow.keras.preprocessing.image import img_to_array
from tensorflow.keras.applications.resnet50 import preprocess_input

# models
from tensorflow.keras.applications.resnet50 import ResNet50
from tensorflow.keras.models import Model

# clustering and dimension reduction
from sklearn.cluster import *
from sklearn.decomposition import PCA
from sklearn.mixture import GaussianMixture
from sklearn.preprocessing import scale

# for everything else
import os
import numpy as np
import matplotlib.pyplot as plt
from random import randint
import pandas as pd
import pickle
import pathlib


def extract_features(file, model):
    # load the image as a 224x224 array
    img = load_img(file, target_size=(224, 224))
    # convert from 'PIL.Image.Image' to numpy array
    img = img_to_array(img)
    # reshape the data for the model reshape(num_of_samples, dim 1, dim 2, channels)
    img = np.expand_dims(img, axis=0)
    # reshaped_img = img.reshape(1,224,224,3)
    # prepare image for model
    imgx = preprocess_input(img)
    # get the feature vector
    features = model.predict(imgx, use_multiprocessing=True)
    features = np.array(features)
    features = features.flatten()
    return features


# function that lets you view a cluster (based on identifier)
def view_cluster(data, cluster, cls, nn):
    plt.figure(figsize=(25, 25))
    plt.gcf().suptitle(
        "cluster class={}, n={}".format(cls, cluster), fontsize=26, c="red"
    )
    # gets the list of filenames for a cluster
    files = data[(data["labels"] == cluster) & (data["class"] == cls)]["files"]
    # only allow up to 30 images to be shown at a time
    print(f"class:{cls}, cluster:{cluster}, size:{len(files)}")
    if len(files) > 100:
        files = files[:100]
    # plot each image in the cluster
    for index, file in enumerate(files):
        plt.subplot(10, 10, index + 1)
        img = load_img(file)
        img = np.array(img)
        plt.imshow(img)
        plt.axis("off")
    plt.tight_layout()
    plt.savefig(
        "painel_cluster_{}_label{}_{}.png".format(nn, cluster, cls),
        facecolor="white",
        transparent=False,
    )


import tensorflow as tf

gpus = tf.config.experimental.list_physical_devices("GPU")
if gpus:
    try:
        # Currently, memory growth needs to be the same across GPUs
        for gpu in gpus:
            tf.config.experimental.set_memory_growth(gpu, True)
        logical_gpus = tf.config.experimental.list_logical_devices("GPU")
        print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPUs")
    except RuntimeError as e:
        # Memory growth must be set before GPUs have been initialized
        print(e)
tf.test.gpu_device_name()

dirname = r"/misc/users/user/covid-net/tools/full_dataset"
dirname = pathlib.Path(dirname)

files = dirname.rglob("*.png")
files = list(files)

model = ResNet50(include_top=False)

# VGG16 feature shape — (1L, 7L, 7L, 512L)
# VGG19 feature shape — (1L, 7L, 7L, 512L)
# InceptionV3 feature shape — (1L, 5L, 5L, 2048L)
# ResNet50 feature shape — (1L, 1L, 1L, 2048L)

# model = VGG16()
# model = Model(inputs = model.inputs, outputs = model.layers[-2].output)
nn = "ResNet50"
model_features = f"features_{nn}.feather"
txt_clusters = f"clusters_{nn}_labels.txt"

data = pd.DataFrame()
cur_dir = os.getcwd()
p = dirname / model_features

if p.is_file():
    data = pd.read_feather(p)
else:
    # lop through each image in the dataset
    for f in files:
        # try to extract the features and update the dictionary
        try:
            feature = extract_features(f, model)
            data[f.relative_to(cur_dir)] = feature
        # if something fails, save the extracted features as a pickle file (optional)
        except:
            data.columns = data.columns.astype(str)
            data.to_feather(p)
    data.columns = data.columns.astype(str)
    data.to_feather(p)

print(
    "number of files: {}, dict size: {}".format(
        len(list(dirname.rglob("*.png"))), len(data.columns)
    )
)
print("data shape:", data.shape)

# get a list of the filenames
filenames = np.array(data.columns)

# get a list of just the features
features = scale(np.array(data).T)
print("features shape:", features.shape)

# reduce the amount of dimensions in the feature vector
pca = PCA(n_components=3000, random_state=22)
pca.fit(features)
x = pca.transform(features)
print("Explained Variance:", pca.explained_variance_ratio_)
print("Variance sum:", sum(pca.explained_variance_ratio_))

# cluster feature vectors
model = KMeans(n_clusters=3, n_jobs=-1)
# model = AgglomerativeClustering(n_clusters=3, linkage="complete", affinity='euclidean')
# model = AgglomerativeClustering(n_clusters=3, linkage="single", affinity='euclidean')
# model = AgglomerativeClustering(n_clusters=3, linkage="ward", affinity='euclidean')
# model = GaussianMixture(n_components=3)
# bandwidth = estimate_bandwidth(x, quantile=0.2, n_samples=500)
# model = MeanShift(bandwidth=bandwidth, bin_seeding=True)
model.fit(x)

data = pd.DataFrame({"files": filenames, "labels": model.labels_})
data["class"] = data["files"].str.split("/").str[1]
data["names"] = data["files"].str.split("/").str[-1]
data[["names", "labels"]].to_csv(
    dirname / txt_clusters, sep=" ", index=False, header=False
)

view_cluster(data, 0, "CP", nn)
view_cluster(data, 1, "CP", nn)
view_cluster(data, 2, "CP", nn)

view_cluster(data, 0, "NCP", nn)
view_cluster(data, 1, "NCP", nn)
view_cluster(data, 2, "NCP", nn)

view_cluster(data, 0, "Normal", nn)
view_cluster(data, 1, "Normal", nn)
view_cluster(data, 2, "Normal", nn)
