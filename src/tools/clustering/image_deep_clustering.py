#!/usr/bin/env python
# coding: utf-8

####################################################################################################
# from: https://towardsdatascience.com/how-to-cluster-images-based-on-visual-similarity-cd6e7209fe34
#
# VGG16 feature shape — (1L, 7L, 7L, 512L)
# VGG19 feature shape — (1L, 7L, 7L, 512L)
# InceptionV3 feature shape — (1L, 5L, 5L, 2048L)
# ResNet50 feature shape — (1L, 1L, 1L, 2048L)
####################################################################################################

# for loading/processing the images
from tensorflow.keras.preprocessing.image import load_img
from tensorflow.keras.preprocessing.image import img_to_array
from tensorflow.keras.applications.resnet50 import preprocess_input

# models
from tensorflow.keras.applications.resnet50 import ResNet50
from tensorflow.keras.models import Model, load_model

from tensorflow.keras import layers
from tensorflow.keras import models
from tensorflow.keras.applications import VGG16

# for everything else
import os
import numpy as np
import matplotlib.pyplot as plt
from random import randint
import pandas as pd
import pickle
import pathlib


def extract_features(file, model):
    # load the image as a 224x224 array
    img = load_img(file, target_size=(512, 512))
    # convert from 'PIL.Image.Image' to numpy array
    img = img_to_array(img) / 255.0
    # reshape the data for the model reshape(num_of_samples, dim 1, dim 2, channels)
    img = np.expand_dims(img, axis=0)

    # reshaped_img = img.reshape(1,224,224,3)
    # prepare image for model
    # imgx = preprocess_input(img)
    # get the feature vector
    predictions = model.predict(img, use_multiprocessing=True)
    predictions = np.array(predictions)
    predictions = predictions.flatten()
    return predictions


import tensorflow as tf

gpus = tf.config.experimental.list_physical_devices("GPU")
if gpus:
    try:
        # Currently, memory growth needs to be the same across GPUs
        for gpu in gpus:
            tf.config.experimental.set_memory_growth(gpu, True)
        logical_gpus = tf.config.experimental.list_logical_devices("GPU")
        print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPUs")
    except RuntimeError as e:
        # Memory growth must be set before GPUs have been initialized
        print(e)
tf.test.gpu_device_name()


dirname = r"/misc/users/user/covid-net/tools/full_dataset"
dirname = pathlib.Path(dirname)

files = dirname.rglob("*.png")
files = list(files)


baseModel = VGG16(
    weights="imagenet",
    include_top=False,
    input_tensor=layers.Input(shape=(512, 512, 3)),
)

headModel = baseModel.output
headModel = layers.AveragePooling2D(pool_size=(7, 7))(headModel)
headModel = layers.Flatten(name="flatten")(headModel)
headModel = layers.Dense(256, activation="relu")(headModel)
headModel = layers.Dropout(0.5)(headModel)
headModel = layers.Dense(4, activation="softmax")(headModel)

model = models.Model(inputs=baseModel.input, outputs=headModel)

model.load_weights("VGG16_fine_tunned_for_clustering_1.h5")

model.summary()

# VGG16 feature shape — (1L, 7L, 7L, 512L)
# VGG19 feature shape — (1L, 7L, 7L, 512L)
# InceptionV3 feature shape — (1L, 5L, 5L, 2048L)
# ResNet50 feature shape — (1L, 1L, 1L, 2048L)

# model = VGG16()
# model = Model(inputs = model.inputs, outputs = model.layers[-2].output)
network = "VGG16"
model_features = f"predictions_{network}_1.feather"
txt_clusters = f"predictions_{network}_labels_1.txt"


data = pd.DataFrame()
cur_dir = os.getcwd()
p = dirname / model_features

if p.is_file():
    data = pd.read_feather(p)
else:
    # lop through each image in the dataset
    for f in files:
        # try to extract the features and update the dictionary
        try:
            feature = extract_features(f, model)
            data[f.relative_to(cur_dir)] = feature
        # if something fails, save the extracted features as a pickle file (optional)
        except:
            data.columns = data.columns.astype(str)
            data.to_feather(p)
    data.columns = data.columns.astype(str)
    data.to_feather(p)

print(
    "number of files: {}, dict size: {}".format(
        len(list(dirname.rglob("*.png"))), len(data.columns)
    )
)
print("data shape:", data.shape)


# get a list of the filenames
filenames = np.array(data.columns)

# get a list of just the features
predictions = np.array(data).T
print("predictions shape:", predictions.shape)

predicted_class_indices = np.argmax(predictions, axis=1)
max_prob = np.max(predictions, axis=1)

data = pd.DataFrame({"files": filenames, "labels": predicted_class_indices})
data["class"] = data["files"].str.split("/").str[1]
data["names"] = data["files"].str.split("/").str[-1]
data[["names", "labels"]].to_csv(
    dirname / txt_clusters, sep=" ", index=False, header=False
)

print(
    "{0: 'pulmao_superior', 1: 'imagem_defeito', 2: 'pulmao_meio', 3: 'pulmao_inferior'}"
)
print(data.groupby(["class", "labels"])["names"].count())
