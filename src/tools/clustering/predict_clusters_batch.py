#!/usr/bin/env python
# coding: utf-8

# for loading/processing the images  
from tensorflow.keras.preprocessing.image import load_img, img_to_array
from tensorflow.keras.applications.resnet50 import preprocess_input 
from tensorflow.keras.preprocessing.image import ImageDataGenerator

# models 
from tensorflow.keras.applications.resnet50 import ResNet50 
from tensorflow.keras.models import Model, load_model

from tensorflow.keras import layers
from tensorflow.keras import models
from tensorflow.keras.applications import VGG16

# for everything else
import os
import numpy as np
import matplotlib.pyplot as plt
from random import randint
import pandas as pd
import pickle
import pathlib
import argparse

def get_all_exams_covidx_by_train_type(rootdir, train_type='train'):
    #loading predictions from feather file (processed in another notebook)
    p = pathlib.Path("./processed_df.feather")
    if p.is_file():
        processed_df = pd.read_feather(p)
    else:
        processed_df = pd.DataFrame()
        
    #aggregate all files and get type
    processed_agg = processed_df.groupby(['label', 'patient_id', 'scan_id']).agg({'type': 'max'})
    f = processed_agg.reset_index()

    dirs = []
    for p in pathlib.Path(rootdir).glob('**/*'):
        #just paths with 9 parts
        if p.is_dir() and len(p.parts) == 9:
            
            label, patient_id, scan_id = p.parts[-3], p.parts[-2], p.parts[-1]

            df_type =  f[(f['label'] == label) & 
                         (f['patient_id'] == int(patient_id)) & 
                         (f['scan_id'] == scan_id)]
            if df_type['type'].empty:
                continue
            if (df_type['type'].values)[0] == train_type:
                dirs.append(p)
    print("{} directories found for {}".format(len(dirs), train_type))
    dirs = [str(d) for d in dirs]
    return dirs


def make_predictions_clusters(data_paths):

    """Loading model"""

    baseModel = VGG16(weights='imagenet', include_top=False, input_tensor=layers.Input(shape=(512, 512, 3)))

    headModel = baseModel.output
    headModel = layers.AveragePooling2D(pool_size=(7, 7))(headModel)
    headModel = layers.Flatten(name="flatten")(headModel)
    headModel = layers.Dense(256, activation="relu")(headModel)
    headModel = layers.Dropout(0.5)(headModel)
    headModel = layers.Dense(4, activation="softmax")(headModel)

    model = models.Model(inputs=baseModel.input, outputs=headModel)
    model.load_weights('VGG16_fine_tunned_for_clustering_2.h5')
    model.summary()


    """Data Paths"""
    results_df = pd.DataFrame()
    for i, data_path_str in enumerate(data_paths):
        data_path = pathlib.Path(data_path_str)
        # keras trick - remove last dir and used it in flow_from_directory classes
        last_dir = data_path.absolute().name
        data_path = pathlib.Path(data_path.absolute().parent)


        """Model Predictions (data_path)"""

        pred_datagen = ImageDataGenerator(rescale=1./255)

        pred_generator = pred_datagen.flow_from_directory(
                data_path,
                color_mode="rgb",
                target_size=(512, 512),
                batch_size=1,
                shuffle = False,
                classes=[last_dir],
                class_mode='categorical')

        filepaths = pred_generator.filepaths
        n_samples = len(filepaths)

        print("starting - {}. {}".format(i, data_path))
        pred = model.predict(pred_generator, steps = n_samples)


        """Post process predictions"""

        labels = {0: 'superior_lung', 1: 'bad_image', 2: 'middle_lung', 3: 'inferior_lung'}
        predicted_class_indices = np.argmax(pred,axis=1)
        predicted_class = [labels[k] for k in predicted_class_indices]

        pred_df = pd.DataFrame({'files': filepaths, 
                             'prob_suplung': pred[:,0],
                             'prob_badimg': pred[:,1],
                             'prob_midlung': pred[:,2],
                             'prob_influng': pred[:,3],
                             'pred_lungclass': predicted_class})
        
        if not results_df.empty:
            results_df = pd.concat([results_df, pred_df], ignore_index=True)
        else:
            results_df = pred_df


    print("feather file modifications (rows, columns):")
    print("pred_df shape", results_df.shape)

    p = pathlib.Path("./predictions_clusters.feather")
    if p.is_file():
        data = pd.read_feather(p)
        print("loaded data shape", data.shape)
        data = pd.concat([data, results_df], ignore_index=True)
        print("concated data shape", data.shape)
        data.drop_duplicates(subset=['files'], keep='last', inplace=True)
        data.reset_index(drop=True, inplace=True)
        print("removed duplicates data shape", data.shape)
        data.to_feather(p)
    else:
        results_df.to_feather(p)
        
if __name__ == '__main__':
    import tensorflow as tf
    gpus = tf.config.experimental.list_physical_devices('GPU')
    if gpus:
      try:
        # Currently, memory growth needs to be the same across GPUs
        for gpu in gpus:
          tf.config.experimental.set_memory_growth(gpu, True)
        logical_gpus = tf.config.experimental.list_logical_devices('GPU')
        print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPUs")
      except RuntimeError as e:
        # Memory growth must be set before GPUs have been initialized
        print(e)
    tf.test.gpu_device_name()

    rootdir='/misc/users/tiagonb/covid-net/dataset'
    dirs = get_all_exams_covidx_by_train_type(rootdir, train_type='train')
    make_predictions_clusters(dirs)
