#!/usr/bin/env python
# coding: utf-8

"""
Generate a panel figure with input files
"""

import pathlib

import matplotlib.pyplot as plt
from imgaug.augmentables.bbs import BoundingBox, BoundingBoxesOnImage
import imageio
import imgaug as ia
import imgaug.augmenters as iaa
import numpy as np
import math

dataset_path = "./full_dataset"
file_pattern = "Normal_772_207_*.png"

files = pathlib.Path(dataset_path).rglob(file_pattern)
files = sorted(list(files))


def show_scan_panel(files, bboxes_dict={}, crop=False):
    n = len(files)
    num_columns = 15
    num_rows = math.ceil(n / num_columns)
    print("n={}, cols={}, rows={}".format(n, num_columns, num_rows))

    plt.figure(figsize=(num_columns * 2, num_rows * 2 + 3))
    cls, patient_id, scan_id = files[0].name.split("_")[:-1]
    figname = "painel[{}]_{}_{}_{}".format(n, cls, patient_id, scan_id)
    plt.gcf().suptitle(figname, fontsize=16, c="red")
    for i, image_path in enumerate(files):
        plt.subplot(num_rows, num_columns, i + 1, xticks=[], yticks=[])
        image = imageio.imread(image_path)
        filename = image_path.name
        # image = np.stack((image,)*3, axis=-1)
        if image_path.name in bboxes_dict:
            seq = iaa.Noop()
            bbox = [int(x) for x in bboxes_dict[filename].split(" ")[-4:]]
            bbs = BoundingBoxesOnImage(
                [BoundingBox(x1=bbox[0], x2=bbox[2], y1=bbox[1], y2=bbox[2])],
                shape=image.shape,
            )
            if crop:
                image, bbs = seq(image=image, bounding_boxes=bbs)
                image = bbs.bounding_boxes[0].extract_from_image(image)
                image = ia.imresize_single_image(image, (512, 512))
            else:
                image = bbs.draw_on_image(image, size=4)

        plt.imshow(image, cmap="gray")
        plt.title("{}".format(filename.split("_")[-1]))
        #
    plt.tight_layout()
    # plt.subplots_adjust(wspace=0, hspace=0, left=0, right=1, bottom=0, top=1)
    plt.savefig("{}.png".format(figname), facecolor="white", transparent=False)
    # plt.show()


show_scan_panel(files)
