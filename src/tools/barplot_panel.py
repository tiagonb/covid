#!/usr/bin/env python
# coding: utf-8

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as plticker
from matplotlib.patches import Patch
import matplotlib
import pandas as pd
import pathlib
import math
import argparse
from utils import model_shortname


parser = argparse.ArgumentParser(description="Create barplots for a given dataset")
parser.add_argument(
    "-d",
    "--dataset",
    type=str,
    default="/misc/users/user/covid-net/dataset/NCP/88/1310",
    help="path to images",
)
parser.add_argument("-o", "--output", type=str, default="", help="save to output file")
parser.add_argument(
    "-t", "--figtitle", type=str, default="", help="title of the figure"
)
args = vars(parser.parse_args())

FEATHER_PATH = "./predictions.feather"
LESION_FILE = "/misc/users/user/covid-net/dataset/lesions_slices.csv"


dataset_name = args["dataset"]
suptitle = args["figtitle"]
savefig = args["output"]


# lesions_slices used to mark which slices were confirmed a lesion
lesion_data = pd.read_csv(LESION_FILE)
lesion_data["lesion_key"] = (
    lesion_data["imgpath"].str.split("/").str[-4:].str.join("/").str.split(".").str[-2]
)


# loading predictions from feather file (processed in another notebook)
p = pathlib.Path(FEATHER_PATH)
if p.is_file():
    df = pd.read_feather(p)
else:
    df = pd.DataFrame()


# process strings from feather file and include in the dataframe to help analysis
try:
    df["lesion_key"] = (
        df["files"].str.split("/").str[-4:].str.join("/").str.split(".").str[-2]
    )
    df["data_class"] = df["dataset"].str.split("/").str[-3]
    df["patient_id"] = df["dataset"].str.split("/").str[-2]
    df["scan_id"] = df["dataset"].str.split("/").str[-1]
except:
    df["lesion_key"] = ""
df = pd.merge(df, lesion_data, how="left")
print("imagens without lesion confirmation: ", len(df[df["imgpath"].isnull()]))
# vector mark which image is in lesion file
df["has_lesion"] = df["imgpath"].notnull()
df.drop(["lesion_key", "imgpath"], axis=1, inplace=True)


# loading predictions lung clusters from feather file (processed in another notebook)
p = pathlib.Path("./predictions_clusters.feather")
if p.is_file():
    df_c = pd.read_feather(p)
else:
    df_c = pd.DataFrame()
df = pd.merge(df, df_c, how="left")


# selecting dataset and a list of the models that made predictions to it
df = df[df["dataset"] == dataset_name]
models = df["model"].unique()


# defining number of columns and rows in the matplotlib fig, each graph ocupy 3 columns
n = len(models)
num_columns = 12
columns_for_graph = 3
num_rows = math.ceil(n / (num_columns // columns_for_graph))
print("n={}, cols={}, rows={}".format(n, num_columns, num_rows))
fig = plt.figure(constrained_layout=True, figsize=(num_columns, num_rows * 4))
spec = fig.add_gridspec(num_rows, num_columns)
if suptitle:
    fig.suptitle(suptitle)

# create grid specifications
axs = []
for g in range(n):
    floor_div = g // (num_columns // columns_for_graph)
    re_div = g % (num_columns // columns_for_graph)
    i, j = floor_div, re_div * columns_for_graph
    ax1 = fig.add_subplot(spec[i, (j) : (j + 2)])
    ax2 = fig.add_subplot(spec[i, (j + 2) : (j + 3)])
    axs.append((ax1, ax2))

# iterate in each model
for i, m in enumerate(models):
    model_data = (
        df[df["model"] == m]
        .sort_values(by=["files"], ascending=True)
        .reset_index(drop=True)
    )
    len_data = len(model_data.index)
    model_data["pred_dummy"] = [0.33] * len_data
    model_data["lesion_dummy"] = [0.33] * len_data
    model_data["cluster_dummy"] = [0.33] * len_data

    # recover previus created axes
    ax = axs[i]
    ax1, ax2 = ax[0], ax[1]

    classes_order = (["NCP"] * len_data) + (["CP"] * len_data) + (["Normal"] * len_data)

    # add width=1.0 to remove space between bars
    barplot1 = model_data[["prob_NCP", "prob_CP", "prob_Normal"]].plot(
        kind="barh", stacked=True, legend=False, ax=ax1, width=1.0
    )
    # change patches colors
    children1 = barplot1.get_children()
    barlist1 = filter(lambda x: isinstance(x, matplotlib.patches.Rectangle), children1)
    for c, bar in zip(classes_order, barlist1):
        if c == "CP":
            bar.set_color("blue")
        elif c == "NCP":
            bar.set_color("red")
        else:
            bar.set_color("green")

    # concatenate values in one big vector to help set the correct color afterward
    data_type = ([0] * len_data) + ([1] * len_data) + ([2] * len_data)
    pred_n_lessions = pd.concat(
        [
            model_data["pred_class"],
            model_data["has_lesion"],
            model_data["pred_lungclass"],
        ]
    ).reset_index(drop=True)

    # add width=1.0 to remove space between bars
    barplot2 = model_data[["pred_dummy", "lesion_dummy", "cluster_dummy"]].plot(
        kind="barh", stacked=True, legend=False, ax=ax2, width=1.0
    )
    # change patches colors
    children2 = barplot2.get_children()
    barlist2 = filter(lambda x: isinstance(x, matplotlib.patches.Rectangle), children2)
    for c, dt, bar in zip(pred_n_lessions, data_type, barlist2):
        if dt == 0:
            if c == "CP":
                bar.set_color("blue")
            elif c == "NCP":
                bar.set_color("red")
            else:
                bar.set_color("green")
        elif dt == 1:
            if c:
                bar.set_color("black")
            else:
                bar.set_color("white")
        else:
            if c == "superior_lung":
                bar.set_color("magenta")
            elif c == "middle_lung":
                bar.set_color("yellow")
            elif c == "inferior_lung":
                bar.set_color("cyan")
            else:
                bar.set_color("black")

    # adjusting graph
    if not i % n:
        ax1.set_xlabel("prob(%)")
        ax1.set_ylabel("ct slice")
    ax1.set_ylim(0, len_data)
    ax1.invert_yaxis()
    ax1.set_xlim(0.0, 1)
    try:
        short_name = model_shortname[m]
    except:
        short_name = "no name"
    ax1.set_title(short_name)
    ax1.yaxis.set_major_locator(plticker.MaxNLocator(integer=True))
    ax1.yaxis.set_major_formatter(plticker.ScalarFormatter())
    ax2.set_xticks([], [])
    ax2.set_yticks([], [])
    ax2.invert_yaxis()
    ax2.set_title("prediction")

# custom legend for the graph
legend_elements1 = [
    Patch(facecolor="blue", label="CP"),
    Patch(facecolor="red", label="NCP"),
    Patch(facecolor="green", label="Normal"),
    Patch(facecolor="black", label="Lesion"),
]
fig.legend(
    handles=legend_elements1,
    loc="upper left",
    title="Predictions",
    bbox_to_anchor=(0.85, 0.35),
)
legend_elements2 = [
    Patch(facecolor="magenta", label="Superior"),
    Patch(facecolor="yellow", label="Middle"),
    Patch(facecolor="cyan", label="Inferior"),
]
fig.legend(
    handles=legend_elements2,
    loc="upper left",
    title="Lung Areas",
    bbox_to_anchor=(0.85, 0.2),
)

if savefig:
    plt.savefig(savefig, facecolor="white", transparent=False)
