#!/usr/bin/env python
# coding: utf-8

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as plticker
from matplotlib.patches import Patch
import matplotlib
import pandas as pd
import pathlib
import math
import argparse
from utils import model_shortname


def add_covidnetx_data(df):
    # loading predictions from feather file (processed in another notebook)
    p = pathlib.Path("./processed_df.feather")
    if p.is_file():
        processed_df = pd.read_feather(p)
    else:
        processed_df = pd.DataFrame()

    # processed_df.drop(['filename','label', 'patient_id', 'scan_id', 'slice_id'], axis=1, inplace=True)
    df["file_key"] = (
        df["files"].str.split("/").str[-4:].str.join("/").str.split(".").str[-2]
    )

    # merging with lesion data
    aug_data = pd.merge(df, processed_df, how="left", on="file_key")
    aug_data["has_lesion"].fillna(False, inplace=True)
    print("files dataframe shape augmented", aug_data.shape)

    return aug_data


def add_lung_cluster_data(df):
    # loading predictions lung clusters from feather file (processed in another notebook)
    p = pathlib.Path("./predictions_clusters.feather")
    if p.is_file():
        df_c = pd.read_feather(p)
    else:
        df_c = pd.DataFrame()
    df = pd.merge(df, df_c, how="left")

    return df


def make_barpanels(df, models, figtitle=None, savefig=None):
    # defining number of columns and rows in the matplotlib fig, each graph ocupy 3 columns
    models = sorted(models, key=lambda item: model_shortname[item])
    n = len(models)
    num_columns = 12
    columns_for_graph = 3
    num_rows = math.ceil(n / (num_columns // columns_for_graph))
    print("n={}, cols={}, rows={}".format(n, num_columns, num_rows))
    fig = plt.figure(constrained_layout=True, figsize=(num_columns, num_rows * 4))
    spec = fig.add_gridspec(num_rows, num_columns)
    if figtitle:
        fig.suptitle(figtitle)

    # create grid specifications
    axs = []
    for g in range(n):
        floor_div = g // (num_columns // columns_for_graph)
        re_div = g % (num_columns // columns_for_graph)
        i, j = floor_div, re_div * columns_for_graph
        ax1 = fig.add_subplot(spec[i, (j) : (j + 2)])
        ax2 = fig.add_subplot(spec[i, (j + 2) : (j + 3)])
        axs.append((ax1, ax2))

    # iterate in each model
    for i, m in enumerate(models):
        model_data = (
            df[df["model"] == m]
            .sort_values(by=["files"], ascending=True)
            .reset_index(drop=True)
        )
        len_data = len(model_data.index)
        model_data["pred_dummy"] = [0.33] * len_data
        model_data["lesion_dummy"] = [0.33] * len_data
        model_data["cluster_dummy"] = [0.33] * len_data

        # recover previus created axes
        ax = axs[i]
        ax1, ax2 = ax[0], ax[1]

        classes_order = (
            (["NCP"] * len_data) + (["CP"] * len_data) + (["Normal"] * len_data)
        )

        # add width=1.0 to remove space between bars
        barplot1 = model_data[["prob_NCP", "prob_CP", "prob_Normal"]].plot(
            kind="barh", stacked=True, legend=False, ax=ax1, width=1.0
        )
        # change patches colors
        children1 = barplot1.get_children()
        barlist1 = filter(
            lambda x: isinstance(x, matplotlib.patches.Rectangle), children1
        )
        for c, bar in zip(classes_order, barlist1):
            if c == "CP":
                bar.set_color("blue")
            elif c == "NCP":
                bar.set_color("red")
            else:
                bar.set_color("green")

        # concatenate values in one big vector to help set the correct color afterward
        data_type = ([0] * len_data) + ([1] * len_data) + ([2] * len_data)
        pred_n_lessions = pd.concat(
            [
                model_data["pred_class"],
                model_data["has_lesion"],
                model_data["pred_lungclass"],
            ]
        ).reset_index(drop=True)

        # add width=1.0 to remove space between bars
        barplot2 = model_data[["pred_dummy", "lesion_dummy", "cluster_dummy"]].plot(
            kind="barh", stacked=True, legend=False, ax=ax2, width=1.0
        )
        # change patches colors
        children2 = barplot2.get_children()
        barlist2 = filter(
            lambda x: isinstance(x, matplotlib.patches.Rectangle), children2
        )
        for c, dt, bar in zip(pred_n_lessions, data_type, barlist2):
            if dt == 0:
                if c == "CP":
                    bar.set_color("blue")
                elif c == "NCP":
                    bar.set_color("red")
                else:
                    bar.set_color("green")
            elif dt == 1:
                if c:
                    bar.set_color("black")
                else:
                    bar.set_color("white")
            else:
                if c == "superior_lung":
                    bar.set_color("magenta")
                elif c == "middle_lung":
                    bar.set_color("yellow")
                elif c == "inferior_lung":
                    bar.set_color("cyan")
                else:
                    bar.set_color("black")

        # adjusting graph
        if not i % n:
            ax1.set_xlabel("prob(%)")
            ax1.set_ylabel("ct slice")
        ax1.set_ylim(0, len_data)
        ax1.invert_yaxis()
        ax1.set_xlim(0.0, 1)
        try:
            short_name = model_shortname[m]
        except:
            short_name = "no name"
        ax1.set_title(short_name)
        ax1.yaxis.set_major_locator(plticker.MaxNLocator(integer=True))
        ax1.yaxis.set_major_formatter(plticker.ScalarFormatter())
        ax2.set_xticks([], [])
        ax2.set_yticks([], [])
        ax2.invert_yaxis()
        ax2.set_title("prediction")

    # custom legend for the graph
    legend_elements1 = [
        Patch(facecolor="blue", label="CP"),
        Patch(facecolor="red", label="NCP"),
        Patch(facecolor="green", label="Normal"),
        Patch(facecolor="black", label="Lesion"),
    ]
    fig.legend(
        handles=legend_elements1,
        loc="upper left",
        title="Predictions",
        bbox_to_anchor=(0.85, 0.35),
    )
    legend_elements2 = [
        Patch(facecolor="magenta", label="Superior"),
        Patch(facecolor="yellow", label="Middle"),
        Patch(facecolor="cyan", label="Inferior"),
    ]
    fig.legend(
        handles=legend_elements2,
        loc="upper left",
        title="Lung Areas",
        bbox_to_anchor=(0.85, 0.2),
    )

    if savefig:
        plt.show()
        plt.savefig(savefig, facecolor="white", transparent=False)


if __name__ == "__main__":
    # loading predictions from feather file (processed in another notebook)
    p = pathlib.Path("./predictions_training_set.feather")
    if p.is_file():
        df = pd.read_feather(p)
    else:
        df = pd.DataFrame()

    df = add_covidnetx_data(df)
    df = add_lung_cluster_data(df)
    df["label"] = df["file_key"].str.split("/").str[0]
    df["patient_id"] = df["file_key"].str.split("/").str[1]
    df["scan_id"] = df["file_key"].str.split("/").str[2]

    # selecting dataset and a list of the models that made predictions to it
    # df = df[df['dataset'] == dataset_name]
    models = df["model"].unique()
    datasets = df["dataset"].unique()

    for d in datasets:
        data = df[df["dataset"] == d]
        models = data["model"].unique()
        num_models = len(models)
        try:
            train_type = data["type"].loc[data["type"].first_valid_index()]
        except:
            train_type = "excluded"
        size = len(data.index) / num_models
        try:
            num_lesions = data["has_lesion"].values.sum() / num_models
        except:
            num_lesions = 0
        cls, patient_id, scan_id = (
            data["label"].iloc[0],
            data["patient_id"].iloc[0],
            data["scan_id"].iloc[0],
        )
        print(size, num_lesions)
        figname = "barplot|{:03d}|{:03d}|_{}_{}_{}".format(
            int(size), int(num_lesions), cls, patient_id, scan_id, train_type
        )
        figtitle = "{} - {}".format(figname, train_type)
        savefig = "./barplot/{}_{}.png".format(figname, train_type)
        print(figtitle, savefig)

        make_barpanels(data, models, figtitle, savefig)
