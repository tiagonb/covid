#!/usr/bin/env python
# coding: utf-8

import os
import sys
import cv2
import json
import numpy as np
import math
import matplotlib.pyplot as plt
from keras.preprocessing.image import ImageDataGenerator, load_img

import argparse
import pathlib
import pandas as pd


# selecting dataset and a list of the models that made predictions to it
model_shortname = {
    "/misc/users/user/covid-net/tools/experiments/mlruns/6/da94eba6fec04c908d77f33ea7f1f405/artifacts/model/data/model.h5": "cnd",
    "/misc/users/user/covid-net/tools/experiments/mlruns/6/3728ffeddd1e44b8a8909421793a6d88/artifacts/model/data/model.h5": "cndd",
    "/misc/users/user/covid-net/tools/experiments/mlruns/6/a2e0cca363864022bb95d29362abea83/artifacts/model/data/model.h5": "resnet50",
    "/misc/users/user/covid-net/source/COVIDNet-CT/models/COVIDNet-CT-A": "covid-ct-A",
    "/misc/users/user/covid-net/models/COVID-Net_CT-1_L": "covid-ct-L",
    "/misc/users/user/covid-net/models/COVID-Net_CT-1_S": "covid-ct-S",
}


def add_lesion(df):
    try:
        df["file_key"] = (
            df["files"].str.split("/").str[-4:].str.join("/").str.split(".").str[0]
        )
    except:
        df["file_key"] = ""
    # loading predictions from feather file (processed in another notebook)
    p = pathlib.Path("./processed_df.feather")
    if p.is_file():
        processed_df = pd.read_feather(p)
    else:
        processed_df = pd.DataFrame()

    processed_df.drop(
        ["filename", "label", "patient_id", "scan_id", "slice_id"], axis=1, inplace=True
    )
    aug_data = pd.merge(df, processed_df, how="left", on="file_key")
    aug_data.drop(["file_key", "type"], axis=1, inplace=True)
    aug_data["has_lesion"].fillna(False, inplace=True)
    # aug_data['has_lesion'] = aug_data['has_lesion'].notnull()
    return aug_data


def show_images_with_predictions(df, figtitle=None, savefig=None):
    # sort files in alphanumerical order
    df = df.sort_values(by=["files"], ascending=True).reset_index(drop=True)
    n = len(df.index)
    # build plot grid
    num_columns = 10
    num_rows = math.ceil(n / num_columns)
    print("n={}, cols={}, rows={}".format(n, num_columns, num_rows))
    plt.figure(figsize=(num_columns * 2, num_rows * 2 + 5))
    plt.gcf().subplots_adjust(top=1.1)
    if figtitle:
        plt.gcf().suptitle(figtitle, y=1.00, fontsize=20)
    for i, row in df.iterrows():
        filename = row["files"].split("/")[-1]
        plt.subplot(num_rows, num_columns, i + 1, xticks=[], yticks=[])
        image = load_img(row["files"])
        plt.imshow(image)
        pred_class = row["pred_class"]
        ax = plt.gca()
        if pred_class == "CP":
            c = "blue"
        elif pred_class == "NCP":
            c = "red"
        else:
            c = "green"
        ax.spines["top"].set_color(c)
        ax.spines["top"].set_linewidth(6)
        try:
            if row["has_lesion"]:
                # modify axis spines for lesion images
                ax.spines["right"].set_color(c)
                ax.spines["right"].set_linewidth(6)
        except:
            pass

        plt.title("{}".format(filename), color=c, fontsize=12)
        plt.xlabel(
            "{}:{:04.2f} {}:{:04.2f} {}:{:04.2f}".format(
                "CP",
                row["prob_CP"],
                "NC",
                row["prob_NCP"],
                "No",
                row["prob_Normal"],
                fontsize=10,
            )
        )
    plt.tight_layout()
    plt.show()
    if savefig:
        plt.savefig(savefig, facecolor="white", transparent=False, dpi=70)


def get_predictions_feather(path="./predictions.feather"):
    # loading predictions from feather file (processed in another notebook)
    p = pathlib.Path(path)
    if p.is_file():
        df = pd.read_feather(p)
    else:
        df = pd.DataFrame()

    return df


def process(model_dir, image_dir, suptitle, savefig):
    df = get_predictions_feather()
    results = df[(df["model"] == model_dir) & (df["dataset"] == image_dir)]
    results = add_lesion(results)
    show_images_with_predictions(results, figtitle=suptitle, savefig=savefig)


if __name__ == "__main__":
    """Argument parsing"""

    parser = argparse.ArgumentParser(
        description="Create panel for a given dataset (looking for data in predictions.feather)"
    )
    parser.add_argument(
        "-m", "--load-model", type=str, default="", help="path to model"
    )
    parser.add_argument("-d", "--dataset", type=str, default="", help="path to images")
    parser.add_argument(
        "-o", "--output", type=str, default="", help="save to output file"
    )
    parser.add_argument(
        "-t", "--figtitle", type=str, default="", help="title of the figure"
    )
    args = vars(parser.parse_args())

    model_dir = args["load_model"]
    image_dir = args["dataset"]
    suptitle = args["figtitle"]
    savefig = args["output"]

    process(model_dir, image_dir, suptitle, savefig)
