#!/usr/bin/env python
# coding: utf-8

import os
import sys
import cv2
import json
import numpy as np
import math
import matplotlib.pyplot as plt
from keras.preprocessing.image import ImageDataGenerator, load_img

import argparse
import pathlib
import pandas as pd


# selecting dataset and a list of the models that made predictions to it
model_shortname = {
    "/misc/users/user/covid-net/tools/experiments/mlruns/6/da94eba6fec04c908d77f33ea7f1f405/artifacts/model/data/model.h5": "cnd",
    "/misc/users/user/covid-net/tools/experiments/mlruns/6/3728ffeddd1e44b8a8909421793a6d88/artifacts/model/data/model.h5": "cndd",
    "/misc/users/user/covid-net/tools/experiments/mlruns/6/a2e0cca363864022bb95d29362abea83/artifacts/model/data/model.h5": "resnet50",
    "/misc/users/user/covid-net/source/COVIDNet-CT/models/COVIDNet-CT-A": "covid-ct-A",
    "/misc/users/user/covid-net/models/COVID-Net_CT-1_L": "covid-ct-L",
    "/misc/users/user/covid-net/models/COVID-Net_CT-1_S": "covid-ct-S",
}


def add_lesion(df):
    try:
        df["file_key"] = (
            df["files"].str.split("/").str[-4:].str.join("/").str.split(".").str[0]
        )
    except:
        df["file_key"] = ""
    # loading predictions from feather file (processed in another notebook)
    p = pathlib.Path("./processed_df.feather")
    if p.is_file():
        processed_df = pd.read_feather(p)
    else:
        processed_df = pd.DataFrame()

    processed_df.drop(
        ["filename", "label", "patient_id", "scan_id", "slice_id"], axis=1, inplace=True
    )
    aug_data = pd.merge(df, processed_df, how="left", on="file_key")
    aug_data.drop(["file_key", "type"], axis=1, inplace=True)
    aug_data["has_lesion"].fillna(False, inplace=True)
    # aug_data['has_lesion'] = aug_data['has_lesion'].notnull()
    return aug_data


def add_covidnetx_data(df):
    # loading predictions from feather file (processed in another notebook)
    p = pathlib.Path("./processed_df.feather")
    if p.is_file():
        processed_df = pd.read_feather(p)
    else:
        processed_df = pd.DataFrame()

    # processed_df.drop(['filename','label', 'patient_id', 'scan_id', 'slice_id'], axis=1, inplace=True)
    df["file_key"] = (
        df["files"].str.split("/").str[-4:].str.join("/").str.split(".").str[-2]
    )

    # merging with lesion data
    aug_data = pd.merge(df, processed_df, how="left", on="file_key")
    aug_data["has_lesion"].fillna(False, inplace=True)
    print("files dataframe shape augmented", aug_data.shape)

    return aug_data


def add_lung_cluster_data(df):
    # loading predictions lung clusters from feather file (processed in another notebook)
    p = pathlib.Path("./predictions_clusters.feather")
    if p.is_file():
        df_c = pd.read_feather(p)
    else:
        df_c = pd.DataFrame()
    df = pd.merge(df, df_c, how="left")

    return df


def show_images_with_predictions(df, figtitle=None, savefig=None):
    # sort files in alphanumerical order
    df = df.sort_values(by=["files"], ascending=True).reset_index(drop=True)
    n = len(df.index)
    # build plot grid
    num_columns = 10
    num_rows = math.ceil(n / num_columns)
    print("n={}, cols={}, rows={}".format(n, num_columns, num_rows))
    plt.figure(figsize=(num_columns * 2, num_rows * 2 + 5))
    plt.gcf().subplots_adjust(top=1.1)
    if figtitle:
        plt.gcf().suptitle(figtitle, y=1.00, fontsize=20)
    for i, row in df.iterrows():
        filename = row["files"].split("/")[-1]
        plt.subplot(num_rows, num_columns, i + 1, xticks=[], yticks=[])
        image = load_img(row["files"])
        plt.imshow(image)
        pred_class = row["pred_class"]
        ax = plt.gca()
        if pred_class == "CP":
            c = "blue"
        elif pred_class == "NCP":
            c = "red"
        else:
            c = "green"
        ax.spines["top"].set_color(c)
        ax.spines["top"].set_linewidth(6)
        try:
            if row["has_lesion"]:
                # modify axis spines for lesion images
                ax.spines["right"].set_color(c)
                ax.spines["right"].set_linewidth(6)
            if row["pred_lungclass"] == "inferior_lung":
                ax.spines["left"].set_color("cyan")
                ax.spines["left"].set_linewidth(6)
            elif row["pred_lungclass"] == "middle_lung":
                ax.spines["left"].set_color("yellow")
                ax.spines["left"].set_linewidth(6)
            elif row["pred_lungclass"] == "superior_lung":
                ax.spines["left"].set_color("magenta")
                ax.spines["left"].set_linewidth(6)
            else:
                pass
        except:
            pass

        plt.title("{}".format(filename), color=c, fontsize=12)
        plt.xlabel(
            "{}:{:04.2f} {}:{:04.2f} {}:{:04.2f}".format(
                "CP",
                row["prob_CP"],
                "NC",
                row["prob_NCP"],
                "No",
                row["prob_Normal"],
                fontsize=10,
            )
        )
    plt.tight_layout()
    plt.show()
    if savefig:
        plt.savefig(savefig, facecolor="white", transparent=False, dpi=70)


if __name__ == "__main__":

    # loading predictions from feather file (processed in another notebook)
    p = pathlib.Path("./predictions_testing_set.feather")
    if p.is_file():
        df = pd.read_feather(p)
    else:
        df = pd.DataFrame()

    df = add_covidnetx_data(df)
    df = add_lung_cluster_data(df)
    df["label"] = df["file_key"].str.split("/").str[0]
    df["patient_id"] = df["file_key"].str.split("/").str[1]
    df["scan_id"] = df["file_key"].str.split("/").str[2]

    # selecting dataset and a list of the models that made predictions to it
    # df = df[df['dataset'] == dataset_name]
    # models = df['model'].unique()
    datasets = df["dataset"].unique()

    for d in datasets:
        data = df[df["dataset"] == d]
        models = data["model"].unique()
        for m in models:
            data_model = data[data["model"] == m]
            try:
                train_type = data_model["type"].loc[
                    data_model["type"].first_valid_index()
                ]
            except:
                train_type = "excluded"
            size = len(data_model.index)
            try:
                num_lesions = data_model["has_lesion"].values.sum()
            except:
                num_lesions = 0
            cls, patient_id, scan_id = (
                data_model["label"].iloc[0],
                data_model["patient_id"].iloc[0],
                data_model["scan_id"].iloc[0],
            )
            figname = "pred_panel|{:03d}|{:03d}|_{}_{}_{}".format(
                int(size), int(num_lesions), cls, patient_id, scan_id, train_type
            )
            figtitle = "{} - {} - {}".format(figname, model_shortname[m], train_type)
            savefig = "./panel_with_pred/{}_{}_{}.png".format(
                figname, model_shortname[m], train_type
            )
            print(figtitle, savefig)

            show_images_with_predictions(data_model, figtitle, savefig)
