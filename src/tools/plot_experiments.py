#!/usr/bin/env python
# coding: utf-8

import pathlib
import matplotlib.pyplot as plt
import numpy as np
import math
import pandas as pd


def plot_panel_acc(data, title):
    n = len(data)
    num_columns = 6
    num_rows = math.ceil(n / num_columns)
    print("n={}, cols={}, rows={}".format(n, num_columns, num_rows))

    plt.figure(figsize=(num_columns * 2, num_rows * 2))
    figname = "{} - acc".format(title)
    plt.gcf().suptitle(figname, fontsize=16, c="red")
    for i, d in enumerate(data):
        epochs = d["metrics"]["step"]
        plt.subplot(num_rows, num_columns, i + 1, xticks=[], yticks=[0.5, 1.0])
        acc = d["metrics"]["acc"]
        val_acc = d["metrics"]["val_acc"]
        plt.plot(epochs, acc, "bo", label="Training acc", markersize=2)
        plt.plot(epochs, val_acc, "b-", label="Validation acc", linewidth=0.5)
        plt.title("{}".format(d["tags"]["run_name"]))
        plt.xlabel("{}".format(d["run_id"][:7]))
        plt.ylim(0, 1.0)
        #
    plt.tight_layout()
    lines_labels = [ax.get_legend_handles_labels() for ax in plt.gcf().axes]
    lines, labels = [sum(lol, []) for lol in zip(*lines_labels)]
    plt.gcf().legend(lines[:2], labels[:2], loc="upper right", ncol=2)
    plt.savefig("{}_acc.png".format(title), facecolor="white", transparent=False)
    # plt.show()


def plot_panel_loss(data, title):
    n = len(data)
    num_columns = 6
    num_rows = math.ceil(n / num_columns)
    print("n={}, cols={}, rows={}".format(n, num_columns, num_rows))

    plt.figure(figsize=(num_columns * 2, num_rows * 2))
    figname = "{} - loss".format(title)
    plt.gcf().suptitle(figname, fontsize=16, c="red")
    for i, d in enumerate(data):
        epochs = d["metrics"]["step"]
        plt.subplot(num_rows, num_columns, i + 1, xticks=[], yticks=[0.5, 5.0])
        acc = d["metrics"]["loss"]
        val_acc = d["metrics"]["val_loss"]
        plt.plot(epochs, acc, "bo", label="Training loss", markersize=2)
        plt.plot(epochs, val_acc, "b-", label="Validation loss", linewidth=0.5)
        plt.title("{}".format(d["tags"]["run_name"]))
        plt.xlabel("{}".format(d["run_id"][:7]))
        plt.ylim(0, 5.0)
        #
    plt.tight_layout()
    lines_labels = [ax.get_legend_handles_labels() for ax in plt.gcf().axes]
    lines, labels = [sum(lol, []) for lol in zip(*lines_labels)]
    plt.gcf().legend(lines[:2], labels[:2], loc="upper right", ncol=2)
    plt.savefig("{}_loss.png".format(title), facecolor="white", transparent=False)
    # plt.show()


def process_experiment(experiment_path):
    exp_path = pathlib.Path(experiment_path)
    files = exp_path.rglob("acc")
    experiment_name = (exp_path / "meta.yaml").read_text().split("\n")[3].split(" ")[-1]
    # sorted by creation date
    files = sorted(files, key=lambda p: p.stat().st_ctime)
    data = []
    for f in files:
        d_dict = {"run_id": f.parent.parent.name}
        run_id = f.parent.parent.name
        d = pd.read_csv(f, sep=" ", names=["time", "acc", "step"], header=None)
        d["time"] = pd.to_datetime((d["time"] / 1000.0), unit="s")
        # loss
        d["loss"] = pd.read_csv(
            f.parent / "loss", sep=" ", names=["time", "loss", "step"], header=None
        )["loss"]
        d["val_acc"] = pd.read_csv(
            f.parent / "val_acc",
            sep=" ",
            names=["time", "val_acc", "step"],
            header=None,
        )["val_acc"]
        d["val_loss"] = pd.read_csv(
            f.parent / "val_loss",
            sep=" ",
            names=["time", "val_loss", "step"],
            header=None,
        )["val_loss"]
        test_acc = test_loss = None
        if (f.parent / "test_acc").is_file():
            test_acc = float((f.parent / "test_acc").read_text().split(" ")[1])
        if (f.parent / "test_loss").is_file():
            test_loss = float((f.parent / "test_loss").read_text().split(" ")[1])

        d_dict["test_acc"] = test_acc
        d_dict["test_loss"] = test_loss
        d_dict["metrics"] = d

        # tags
        tag_dir = f.parent.parent / "tags"
        t = {"run_name": ""}
        for tag in tag_dir.glob("*"):
            if "mlflow.runName" in tag.name:
                t["run_name"] = tag.read_text()
                # print("tag_content={}".format(tag.read_text()))
        d_dict["tags"] = t

        data.append(d_dict)
    return data, experiment_name


experiment_path = "/misc/users/user/covid-net/tools/experiments/mlruns/6"
data, experiment_name = process_experiment(experiment_path)

plot_panel_acc(data, experiment_name)
plot_panel_loss(data, experiment_name)
