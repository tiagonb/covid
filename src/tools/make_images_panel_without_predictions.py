import pathlib

import matplotlib.pyplot as plt
from keras.preprocessing.image import load_img
import numpy as np
import math
import pandas as pd


def df_from_dataset_covidx(rootdir, pattern):

    # list of ct scan images to display
    files = pathlib.Path(rootdir).rglob(pattern)
    files = sorted(list(files))
    files_data = pd.DataFrame({"files": files})
    files_data["files"] = files_data["files"].astype("string")
    print("files dataframe shape raw", files_data.shape)
    files_data["file_key"] = (
        files_data["files"]
        .str.split("/")
        .str[-1]
        .str.split(".")
        .str[0]
        .str.split("_")
        .str.join("/")
    )
    files_data["label"] = files_data["file_key"].str.split("/").str[0]
    files_data["patient_id"] = files_data["file_key"].str.split("/").str[1]
    files_data["scan_id"] = files_data["file_key"].str.split("/").str[2]
    files_data["slice_id"] = files_data["file_key"].str.split("/").str[3]
    print("files dataframe shape processed", files_data.shape)

    return files_data


def df_from_dataset_raw(rootdir, pattern):

    # list of ct scan images to display
    files = pathlib.Path(rootdir).rglob(pattern)
    files = sorted(list(files))
    files_data = pd.DataFrame({"files": files})
    files_data["files"] = files_data["files"].astype("string")
    print("files dataframe shape raw", files_data.shape)
    files_data["file_key"] = (
        files_data["files"].str.split("/").str[-4:].str.join("/").str.split(".").str[0]
    )
    files_data["label"] = files_data["file_key"].str.split("/").str[0]
    files_data["patient_id"] = files_data["file_key"].str.split("/").str[1]
    files_data["scan_id"] = files_data["file_key"].str.split("/").str[2]
    files_data["slice_id"] = files_data["file_key"].str.split("/").str[3]
    print("files dataframe shape processed", files_data.shape)

    # merging with lesion data
    aug_data = pd.merge(files_data, processed_df, how="left", on="file_key")
    # filling nan with false
    aug_data["has_lesion"].fillna(False, inplace=True)
    print("files dataframe shape augmented", aug_data.shape)

    return aug_data


def show_images_without_predictions(df, figtitle=None, savefig=None):
    # sort files in alphanumerical order
    df = df.sort_values(by=["files"], ascending=True).reset_index(drop=True)
    n = len(df.index)
    # build plot grid
    num_columns = 10
    num_rows = math.ceil(n / num_columns)
    print("n={}, cols={}, rows={}".format(n, num_columns, num_rows))
    plt.figure(figsize=(num_columns * 2, num_rows * 2 + 5))
    plt.gcf().subplots_adjust(top=1.1)
    df["type"] = df["type"].fillna("")
    if figtitle:
        plt.gcf().suptitle(figtitle, y=1.00, fontsize=20)
    for i, row in df.iterrows():
        filename = row["files"].split("/")[-1]
        plt.subplot(num_rows, num_columns, i + 1, xticks=[], yticks=[])
        image = load_img(row["files"])
        plt.imshow(image)
        ax = plt.gca()
        try:
            if row["has_lesion"]:
                # modify axis spines for lesion images
                ax.spines["right"].set_color("red")
                ax.spines["right"].set_linewidth(6)
        except:
            pass
        plt.title("{}".format(filename), fontsize=12)
        plt.xlabel("{}".format(row["type"], fontsize=10))
    plt.tight_layout()
    # plt.show()
    if savefig:
        plt.savefig(savefig, facecolor="white", transparent=False, dpi=45)
        plt.close()


# loading predictions from feather file (processed in another notebook)
p = pathlib.Path("./processed_df.feather")
if p.is_file():
    processed_df = pd.read_feather(p)
else:
    processed_df = pd.DataFrame()

processed_df.drop(
    ["filename", "label", "patient_id", "scan_id", "slice_id"], axis=1, inplace=True
)

print(processed_df.shape)
processed_df.head()

# traverse all subdirs in rootdir
rootdir = "/misc/users/user/covid-net/dataset"
dirs = []
for p in pathlib.Path(rootdir).glob("**/*"):
    if p.is_dir() and len(p.parts) == 9:
        dirs.append(p)


# make plot
for d in dirs:
    print("processing:", d)
    rootdir = d
    pattern = "*"
    df = df_from_dataset_raw(rootdir, pattern)
    if len(df) == 0:  # empty dir are discarted
        print("empty dir:", d)
        continue
    try:
        train_type = df["type"].loc[df["type"].first_valid_index()]
    except:
        # some scans are in the excluded list, so they don't have train_type info
        train_type = "excluded"
    size = len(df.index)
    try:
        num_lesions = df["has_lesion"].values.sum()
    except:
        num_lesions = 0
    cls, patient_id, scan_id = df["label"][0], df["patient_id"][0], df["scan_id"][0]
    figname = "painel|{:03d}|{:03d}|_{}_{}_{}".format(
        size, num_lesions, cls, patient_id, scan_id, train_type
    )
    figtitle = "{} - {}".format(figname, train_type)
    savefig = "./panel_without_pred/{}_{}.png".format(figname, train_type)

    show_images_without_predictions(df, figtitle, savefig)
